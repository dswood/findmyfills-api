/*
 * MIT License
 *
 * Copyright (c) 2019 Proof Trading, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

// let base_url = 'http://localhost:8080';
let base_url = '';
// let base_url = 'https://prerakbeta-dot-findmyfills-api.appspot.com';

window.addEventListener('load', function () {

    document.getElementById('sign-out').onclick = function () {
        firebase.auth().signOut();
    };

    document.getElementById('fetch-data').onclick = function () {
        fetch(base_url + '/chart/sec/BAC/1559827800000/1559827920000/', {
            method: 'GET',
            credentials: 'include'
        }).then(res => {
            if (res.status === 200) {
                res.json().then(function (payload) {
                    document.getElementById('chart-data').innerText = JSON.stringify(payload, null, 4);
                })
            } else {
                res.text().then(function (payload) {
                    const {error} = payload;
                    new Error(error)
                })
            }
        })
    };

    // FirebaseUI config.
    let uiConfig = {
        signInSuccessUrl: '/login/index.html',
        credentialHelper: firebaseui.auth.CredentialHelper.NONE,
        signInOptions: [
            // Remove any lines corresponding to providers you did not check in
            // the Firebase console.
            firebase.auth.GoogleAuthProvider.PROVIDER_ID,
            firebase.auth.EmailAuthProvider.PROVIDER_ID,
        ],
        // Terms of service url.
        tosUrl: '<your-tos-url>'
    };

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            // User is signed in, so display the "sign out" button and login info.
            document.getElementById('sign-out').hidden = false;
            document.getElementById('login-info').hidden = false;
            let msg = `Signed in as ${user.displayName} (${user.email})`;
            document.getElementById('message').innerText = msg;
            console.log(msg);
            user.getIdToken().then(function (token) {
                fetch(base_url + '/initSession', {
                    method: 'POST',
                    credentials: 'include',
                    body: JSON.stringify({
                        'id_token': token
                    })
                }).then(res => {
                    if (res.status === 200) {
                        res.text().then(function (payload) {
                            console.log(`Server initSession said: ${payload}`)
                        })
                    } else {
                        res.text().then(function (payload) {
                            const {error} = payload;
                            new Error(error)
                        })
                    }
                })
            });
        } else {
            // User is signed out.
            // Clear out our stored cookie
            fetch(base_url + '/endSession', {
                method: 'POST'
            }).then(r => r.text().then(function (payload) {
                console.log(`Server endSession said: ${payload}`)
            }));
            // Initialize the FirebaseUI Widget using Firebase.
            let ui = new firebaseui.auth.AuthUI(firebase.auth());
            // Show the Firebase login button.
            ui.start('#firebaseui-auth-container', uiConfig);
            // Update the login state indicators.
            document.getElementById('sign-out').hidden = true;
            document.getElementById('login-info').hidden = true;
        }
    }, function (error) {
        console.log(error);
        alert('Unable to log in: ' + error)
    });
});

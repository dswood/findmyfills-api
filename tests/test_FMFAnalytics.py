#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#

import datetime
import logging
import os
from unittest import TestCase

import numpy as np
import pandas as pd

from analytics.engine import FillView, Side, FMFAnalytics
from logging_helper import init_logging
from marketdata.polygon import PolygonDataSource, unmap_json_fields
from tests.test_polygonDataSource import connects_to_polygon_io
from util import translate_to_datetime64, del_fields

init_logging(level_name='DEBUG')
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 2000)

LOG = logging.getLogger(__name__)


class TestFillView(FillView):
    def get_time(self, fill) -> np.datetime64:
        return fill[0]

    def get_symbol(self, fill) -> str:
        return fill[1]

    def get_side(self, fill) -> Side:
        return fill[2]

    def get_price(self, fill) -> float:
        return fill[3]

    def get_size(self, fill) -> int:
        return fill[4]

    def get_last_mkt(self, fill) -> str:
        return fill[5]


class TestFMFAnalytics(TestCase):

    def setUp(self):
        test_cache_dir = os.path.join(os.path.dirname(__file__), 'test_data')
        self.ds = PolygonDataSource(cache_dir=test_cache_dir)
        self.fmfa = FMFAnalytics(self.ds, TestFillView())

    def test_summarize_buy(self):
        f1 = self.new_fill('2019-06-14T09:45:00.172', 'AAPL', Side.buy, 10.00, 100, 'XNGS')
        f2 = self.new_fill('2019-06-14T09:46:00.172', 'AAPL', Side.buy, 10.01, 300, 'XNGS')
        f3 = self.new_fill('2019-06-14T09:47:00.172', 'AAPL', Side.buy, 10.02, 200, 'XNGS')
        summary = self.fmfa.summarize([f1, f2, f3], 10_0000, 10_0100, 10_0200, 10_0000)
        LOG.info("summary=%s", summary)
        self.assertEqual(600, summary['volume'])
        self.assertEqual(3, summary['count'])
        self.assertEqual(200, summary['avg_size'])
        self.assertEqual(10_0116, summary['avg_price'])
        self.assertEqual(10_0000, summary['arrival'])
        self.assertEqual(10_0100, summary['vwap'])
        self.assertEqual(10_0200, summary['end_midpoint'])
        self.assertEqual(10_0000, summary['reversion_px'])
        self.assertEqual(116, summary['slippage_arrival'])
        self.assertEqual(16, summary['slippage_vwap'])
        self.assertEqual(200, summary['reversion'])

    def test_summarize_sell(self):
        f1 = self.new_fill('2019-06-14T09:45:00.172', 'AAPL', Side.sell, 10.00, 100, 'XNGS')
        f2 = self.new_fill('2019-06-14T09:46:00.172', 'AAPL', Side.sell, 10.01, 300, 'XNGS')
        f3 = self.new_fill('2019-06-14T09:47:00.172', 'AAPL', Side.sell, 10.02, 200, 'XNGS')
        summary = self.fmfa.summarize([f1, f2, f3], 10_0000, 10_0100, 10_0200, 10_0000)
        LOG.info("summary=%s", summary)
        self.assertEqual(600, summary['volume'])
        self.assertEqual(3, summary['count'])
        self.assertEqual(200, summary['avg_size'])
        self.assertEqual(10_0116, summary['avg_price'])
        self.assertEqual(10_0000, summary['arrival'])
        self.assertEqual(10_0100, summary['vwap'])
        self.assertEqual(10_0200, summary['end_midpoint'])
        self.assertEqual(10_0000, summary['reversion_px'])
        self.assertEqual(-116, summary['slippage_arrival'])
        self.assertEqual(-16, summary['slippage_vwap'])
        self.assertEqual(-200, summary['reversion'])

    def test_summarize_bad_benchmarks(self):
        f1 = self.new_fill('2019-06-14T09:45:00.172', 'AAPL', Side.buy, 10.00, 100, 'XNGS')
        f2 = self.new_fill('2019-06-14T09:46:00.172', 'AAPL', Side.buy, 10.01, 300, 'XNGS')
        f3 = self.new_fill('2019-06-14T09:47:00.172', 'AAPL', Side.buy, 10.02, 200, 'XNGS')
        summary = self.fmfa.summarize([f1, f2, f3], None, None, None, None)
        LOG.info("summary=%s", summary)
        self.assertEqual(600, summary['volume'])
        self.assertEqual(3, summary['count'])
        self.assertEqual(200, summary['avg_size'])
        self.assertEqual(10_0116, summary['avg_price'])
        self.assertEqual(None, summary['arrival'])
        self.assertEqual(None, summary['vwap'])
        self.assertEqual(None, summary['end_midpoint'])
        self.assertEqual(None, summary['reversion_px'])
        self.assertEqual(None, summary['slippage_arrival'])
        self.assertEqual(None, summary['slippage_vwap'])
        self.assertEqual(None, summary['reversion'])

    def test_enrich(self):
        trades = self.ds.load_json('trades_fullday_CBOE.json.gz')
        trades = unmap_json_fields(trades, self.ds.format_trade)
        translate_to_datetime64(trades, 'timestamp')

        f1 = self.new_fill('2019-06-14T09:33:23.000', 'CBOE', Side.buy, 107.15, 100, 'XNGS')
        f2 = self.new_fill('2019-06-14T09:34:30.000', 'CBOE', Side.buy, 107.10, 100, 'XNGS')
        f3 = self.new_fill('2019-06-14T09:35:00.000', 'CBOE', Side.buy, 106.95, 100, 'XNGS')
        fills = [f1, f2, f3]
        nbbos = self.fmfa.get_nbbos_asof('CBOE', [self.fmfa.get_fill_markout_time(f) for f in fills])
        enriched = self.fmfa.enrich(fills, nbbos)
        for fill, data in enriched:
            LOG.info("%s - %s", fill, data)

    @connects_to_polygon_io
    def test_calculate(self):
        self.run_calculate_test(False)

    @connects_to_polygon_io
    def test_calculate_parallel(self):
        self.run_calculate_test(True)

    def run_calculate_test(self, parallel):
        t1 = datetime.datetime.now()
        self.fmfa.parallel_asof = parallel
        f1 = self.new_fill('2019-06-14T09:33:23.000', 'CBOE', Side.buy, 107.15, 100, 'XNGS')
        f2 = self.new_fill('2019-06-14T09:34:30.000', 'CBOE', Side.buy, 107.10, 1000, 'XNGS')
        f3 = self.new_fill('2019-06-14T09:35:00.000', 'CBOE', Side.buy, 106.95, 100, 'XNGS')
        summary, enriched_fills = self.fmfa.calculate([f1, f2, f3])
        LOG.info(summary)
        for fill, data in enriched_fills:
            LOG.info("%s - %s", fill, data)
        self.assertEqual(1072950, summary['arrival'])
        self.assertEqual(1071350, summary['end_midpoint'])
        self.assertEqual(1068800, summary['reversion_px'])
        self.assertEqual(1071200, enriched_fills[0][1]['markout_px'])
        self.assertEqual(1071250, enriched_fills[1][1]['markout_px'])
        self.assertEqual(1068800, enriched_fills[2][1]['markout_px'])
        LOG.info("run_calcuate_test(%s) duration=%s", parallel, datetime.datetime.now() - t1)

    def get_trades_v2_for_test(self, sym, dt_start_str, dt_end_str, limit=5000):
        file_name = "trades_v2_%s.json" % sym
        trades_raw = self.ds.load_json(file_name)
        if trades_raw is None:
            dt_start = datetime.datetime.strptime(dt_start_str, '%Y-%m-%d %H:%M:%S')
            dt_end = datetime.datetime.strptime(dt_end_str, '%Y-%m-%d %H:%M:%S')
            trades_raw = self.ds.get_trades_v2(sym, dt_start, dt_end, limit, False)
            self.ds.save_json(file_name, trades_raw)
        trades = unmap_json_fields(trades_raw, self.ds.format_trade_v2)
        del_fields(trades,
                   'trf_timestamp', 'trf_id', 'id', 'orig_id',
                   'sip_timestamp', 'correction', 'tape', 'conds')
        # copy time for debugging
        for trade in trades:
            trade['time'] = trade['participant_timestamp']
        translate_to_datetime64(trades, 'time')
        return trades

    def run_find_trades_for_fill_test(self, fill, trades, expected_matches):
        matches = self.fmfa.find_trades_for_fill(fill, trades, pd.DataFrame(trades), 1000, 'participant_timestamp')
        LOG.info("fill=%s", fill)
        LOG.info("matches(%d)\n%s", len(matches), pd.DataFrame(matches))
        self.assertEqual(expected_matches, len(matches))
        for match in matches:
            self.assertEqual(match['size'], self.fmfa.fill_view.get_size(fill))
            self.assertAlmostEqual(match['price'], self.fmfa.fill_view.get_price(fill), 3)
        return matches

    def test_find_trades_for_fill(self):
        trades = self.get_trades_v2_for_test('IBKR', '2019-07-05 09:45:00', '2019-07-05 09:55:00')

        # basic single match
        fill = self.new_fill('2019-07-05T09:46:24.000', 'IBKR', Side.buy, 53.96, 100, 'BATY')
        self.run_find_trades_for_fill_test(fill, trades, 1)

        # outside of time (early) trade_time=2019-07-05 13:46:24.111308032
        fill = self.new_fill('2019-07-05T09:46:23.000', 'IBKR', Side.buy, 53.96, 100, 'BATY')
        self.run_find_trades_for_fill_test(fill, trades, 0)

        # outside of time (late)
        fill = self.new_fill('2019-07-05T09:46:26.000', 'IBKR', Side.buy, 53.96, 100, 'BATY')
        self.run_find_trades_for_fill_test(fill, trades, 0)

        # size diff
        fill = self.new_fill('2019-07-05T09:46:24.000', 'IBKR', Side.buy, 53.96, 200, 'BATY')
        self.run_find_trades_for_fill_test(fill, trades, 0)

        # price diff
        fill = self.new_fill('2019-07-05T09:46:24.000', 'IBKR', Side.buy, 53.83, 100, 'BATY')
        self.run_find_trades_for_fill_test(fill, trades, 0)

        # no trades
        fill = self.new_fill('2019-07-05T09:46:24.000', 'IBKR', Side.buy, 53.83, 100, 'BATY')
        self.run_find_trades_for_fill_test(fill, [], 0)

    def test_find_trades_for_fill_last_mkt(self):
        """
        43      EDGA     v 2019-07-05 13:47:45.587700992  53.9200   100   IBKR
        44      XPHL     v 2019-07-05 13:47:45.640027904  53.9200   100   IBKR
        :return:
        """
        trades = self.get_trades_v2_for_test('IBKR', '2019-07-05 09:45:00', '2019-07-05 09:55:00')
        # translate_to_datetime64(trades, 'participant_timestamp')
        # LOG.info("\n%s", pd.DataFrame(trades))

        # match finds 2 fills w/out last market
        fill = self.new_fill('2019-07-05T09:47:45.000', 'IBKR', Side.buy, 53.92, 100, None)
        self.run_find_trades_for_fill_test(fill, trades, 2)

        # match 2nd fill based on last market
        fill = self.new_fill('2019-07-05T09:47:45.000', 'IBKR', Side.buy, 53.92, 100, 'XPHL')
        matches = self.run_find_trades_for_fill_test(fill, trades, 1)
        self.assertTrue(matches[0]['exchange'] == 'XPHL')

    def test_tape_match(self):
        trades = self.get_trades_v2_for_test('IBKR', '2019-07-05 09:45:00', '2019-07-05 09:55:00')
        fill = self.new_fill('2019-07-05T09:46:24.000', 'IBKR', Side.buy, 53.96, 100, 'BATY')
        summary, enriched_fills = self.fmfa.tape_match([fill], trades)
        LOG.info("summary=%s", summary)
        for fill, matches in enriched_fills:
            LOG.info("fill=%s", fill)
            LOG.info("matches(%d)=%s", len(matches), matches)
            self.assertEqual(1, len(matches))
            self.assertAlmostEqual(53.96, matches[0]['price'], 4)
            self.assertEqual(100, matches[0]['size'])
            self.assertEqual('BATY', matches[0]['exchange'])

    def test_tape_match_no_trades(self):
        fill = self.new_fill('2019-06-13T11:00:25.000', 'UBNT', Side.buy, 132.59, 23, None)
        summary, enriched_fills = self.fmfa.tape_match([fill], [])
        LOG.info("summary=%s", summary)
        for fill, matches in enriched_fills:
            LOG.info("fill=%s", fill)
            LOG.info("matches=%s", matches)

    def test_tape_match_dupe_fills(self):
        """
        2 fills for 2 identical trades...
        for both fills, return the 2 trades but in a different order for 2nd match
        Allows different trades to be returned as (first) match while allowing stats on multi-match
        """
        # 87   XNGS   pcv   1.562335e+12   53.8700    2271301   100   IBKR 2019-07-05 13:48:52.829251584        0
        # 88   XNGS   pcv   1.562335e+12   53.8700    2271401   100   IBKR 2019-07-05 13:48:52.829445888        0
        f1 = self.new_fill('2019-07-05T09:48:52.000', 'IBKR', Side.buy, 53.87, 100, 'XNGS')
        f2 = self.new_fill('2019-07-05T09:48:52.000', 'IBKR', Side.buy, 53.87, 100, 'XNGS')
        summary, enriched_fills = self.do_tape_match([f1, f2])

        # both fills are matches so they are returned but some state is saved about the first fill
        fill1, matches1 = enriched_fills[0]
        self.assertEqual(2, len(matches1))
        self.assertEqual(2271301, matches1[0]['sequence_number'])
        self.assertEqual(2271401, matches1[1]['sequence_number'])
        # for 2nd match, both fills are returned, but the order is different
        fill2, matches2 = enriched_fills[1]
        self.assertEqual(2, len(matches2))
        self.assertEqual(2271401, matches2[0]['sequence_number'])
        self.assertEqual(2271301, matches2[1]['sequence_number'])

    def test_tape_match_multi_fills_overlap(self):
        """
        fill with valid venue is tried first which narrows results for 2nd fill w/o last mkt
        assuming we don't get last market, this is a secondary use case
        """
        # 43   EDGA   pcv   1.562334e+12   53.9200    2164201   100   IBKR 2019-07-05 13:47:45.587700992        0
        # 44   XPHL   pcv   1.562334e+12   53.9200    2164301   100   IBKR 2019-07-05 13:47:45.640027904        0
        f1 = self.new_fill('2019-07-05T09:47:45.000', 'IBKR', Side.buy, 53.92, 100, None)
        f2 = self.new_fill('2019-07-05T09:47:45.000', 'IBKR', Side.buy, 53.92, 100, 'XPHL')
        summary, enriched_fills = self.do_tape_match([f1, f2])

        # first match doesn't have a valid last market so 2 fills are returned
        fill1, matches1 = enriched_fills[0]
        self.assertEqual(1, len(matches1))
        self.assertEqual('EDGA', matches1[0]['exchange'])
        # second match is exact w/last market
        fill2, matches2 = enriched_fills[1]
        self.assertEqual(1, len(matches2))
        self.assertEqual('XPHL', matches2[0]['exchange'])

    def test_tape_match_2_fills_1_trade(self):
        """
        2 fills exactly match single trade...fill is duped or time issue
        f1 = t1, f2 = []
        """
        # 103     BATY    cv     1.562335e+12  53.6000          2642201     1   IBKR 2019-07-05 13:53:05.392915968
        # 104     BATY    cv     1.562335e+12  53.6000          2652401     1   IBKR 2019-07-05 13:53:18.442917120
        # 105     BATY    cv     1.562335e+12  53.6000          2677301     1   IBKR 2019-07-05 13:53:38.440749056
        f1 = self.new_fill('2019-07-05T09:53:18.000', 'IBKR', Side.buy, 53.71, 1, None)
        f2 = self.new_fill('2019-07-05T09:53:18.000', 'IBKR', Side.buy, 53.71, 1, None)
        summary, enriched_fills = self.do_tape_match([f1, f2])

        # first exact match
        fill1, matches1 = enriched_fills[0]
        self.assertEqual(1, len(matches1))
        self.assertEqual('BATY', matches1[0]['exchange'])
        # second match is excluded cuz prev is exact
        fill2, matches2 = enriched_fills[1]
        self.assertEqual(0, len(matches2))

    def test_tape_match_3_fills_2_trade_partial(self):
        """
        3 fills that all match the same 2 trades...could happend if data is invalid or times is off
        f1 = t1,t2; f2 = t2,t1; f3 = t1,t2 (??)
        """
        # 11      IEXG   pcv     1.562334e+12  53.7350          2120301   100   IBKR 2019-07-05 13:47:18.830501120
        # 12      IEXG   pcv     1.562334e+12  53.7350          2120401   100   IBKR 2019-07-05 13:47:18.831564800
        # 13      IEXG   pcv     1.562334e+12  53.6800          2123301   100   IBKR 2019-07-05 13:47:19.965846016
        # 14      BATS   pcv     1.562334e+12  53.7350          2126301   100   IBKR 2019-07-05 13:47:23.028623872
        # 15      XNGS   pcv     1.562334e+12  53.7350          2126401   101   IBKR 2019-07-05 13:47:23.029627648
        f1 = self.new_fill('2019-07-05T09:47:18.000', 'IBKR', Side.buy, 53.7350, 100, None)
        f2 = self.new_fill('2019-07-05T09:47:18.000', 'IBKR', Side.buy, 53.7350, 100, None)
        f3 = self.new_fill('2019-07-05T09:47:18.000', 'IBKR', Side.buy, 53.7350, 100, None)
        summary, enriched_fills = self.do_tape_match([f1, f2, f3])

        # f1 = t1, t2
        fill1, matches1 = enriched_fills[0]
        self.assertEqual(2, len(matches1))
        self.assertEqual(2120301, matches1[0]['sequence_number'])
        self.assertEqual(2120401, matches1[1]['sequence_number'])
        # f2 = t2, t1
        fill2, matches2 = enriched_fills[1]
        self.assertEqual(2, len(matches2))
        self.assertEqual(2120401, matches2[0]['sequence_number'])
        self.assertEqual(2120301, matches2[1]['sequence_number'])
        # f3 = t1, t2 (again)
        fill3, matches3 = enriched_fills[2]
        self.assertEqual(2, len(matches3))
        self.assertEqual(2120301, matches3[0]['sequence_number'])
        self.assertEqual(2120401, matches3[1]['sequence_number'])

    def do_tape_match(self, fills):
        trades = self.get_trades_v2_for_test('IBKR', '2019-07-05 09:45:00', '2019-07-05 09:55:00')
        LOG.info(f"\n{pd.DataFrame(trades)}")
        summary, enriched_fills = self.fmfa.tape_match(fills, trades)
        LOG.info("summary=%s", summary)
        for fill, matches in enriched_fills:
            LOG.info("fill=%s", fill)
            LOG.info("matches(%d)=%s", len(matches), matches)
        return summary, enriched_fills

    def test_mid(self):
        # normal
        mid = FMFAnalytics.calc_mid({"bid_price": 1.00, "ask_price": 1.20})
        self.assertEqual(1_1000, mid)
        # missing
        mid = FMFAnalytics.calc_mid(None)
        self.assertEqual(None, mid)
        # crossed
        mid = FMFAnalytics.calc_mid({"bid_price": 2.00, "ask_price": 1.80})
        self.assertEqual(1_9000, mid)
        # one-sided
        mid = FMFAnalytics.calc_mid({"bid_price": None, "ask_price": 2})
        self.assertEqual(2_0000, mid)
        mid = FMFAnalytics.calc_mid({"bid_price": 1, "ask_price": None})
        self.assertEqual(1_0000, mid)
        # stub
        mid = FMFAnalytics.calc_mid({"bid_price": 0.01, "ask_price": 200_000})
        self.assertEqual(None, mid)
        mid = FMFAnalytics.calc_mid({"bid_price": 200_000, "ask_price": 100_000})
        self.assertEqual(None, mid)
        mid = FMFAnalytics.calc_mid({"bid_price": 1, "ask_price": 1001.01})
        self.assertEqual(None, mid)
        mid = FMFAnalytics.calc_mid({"bid_price": 10.00, "ask_price": 200_000})
        self.assertEqual(None, mid)
        mid = FMFAnalytics.calc_mid({"bid_price": 0.0001, "ask_price": 10.00})
        self.assertEqual(None, mid)

    @staticmethod
    def new_fill(time, sym, side, price, size, last_mkt):
        time = pd.to_datetime(time).tz_localize('US/Eastern').tz_convert('UTC').tz_localize(None).to_datetime64()
        return [time, sym, side, price, size, last_mkt]

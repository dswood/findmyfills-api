#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import logging
import numpy as np
import pandas as pd
import datetime
from util import *
from unittest import TestCase

from logging_helper import init_logging

init_logging()
LOG = logging.getLogger(__name__)


class UtilTest(TestCase):
    TEST_DATE_TIME_STR = '2019-07-05T09:30:00.000-0400'
    TEST_DATE_STR = '2019-07-05'
    # new Date(1562333400000) => Fri Jul 05 2019 09:30:00 GMT-0400 (Eastern Daylight Time)
    TEST_EPOCH_TIME_MS = 1562333400000
    TEST_EPOCH_TIME_NS = TEST_EPOCH_TIME_MS * 1000 * 1000

    def test_datetime_str_and_ns(self):
        dt = datetime.datetime.strptime(UtilTest.TEST_DATE_TIME_STR, '%Y-%m-%dT%H:%M:%S.%f%z')
        self.run_datetime_test(dt)

    def test_timestamp_str_and_ns(self):
        ts = pd.Timestamp(UtilTest.TEST_DATE_TIME_STR)
        self.run_datetime_test(ts)

    def test_datetime64_str_and_ns(self):
        # works but will cause deprecation warning
        d64 = np.datetime64(UtilTest.TEST_DATE_TIME_STR)
        self.run_datetime_test(d64)

    def run_datetime_test(self, dt):
        dt_str = get_date_string(dt, '%Y-%m-%d')
        ns = get_time_in_ns(dt)
        LOG.info("dt=%s, ns=%d, date_str=%s %s", dt, ns, dt_str, type(dt))
        self.assertEqual(UtilTest.TEST_DATE_STR, dt_str)
        self.assertEqual(UtilTest.TEST_EPOCH_TIME_NS, ns)

    def test_parallel_exec(self):
        def times2(x):
            return x * 2

        # single
        res = parallel_exec([("id", times2, [3])])
        self.assertEqual(6, res["id"])

        # multiple
        res = parallel_exec([(i, times2, [i]) for i in range(0, 10)])
        for i in range(0, 10):
            self.assertEqual(i * 2, res[i])

        # error returned
        def err(x):
            if x == 2:
                raise ValueError("banana")
            return x
        res = parallel_exec([(i, err, [i]) for i in range(0, 5)], raise_errors=False)
        for i in range(0, 5):
            if i == 2:
                self.assertTrue(isinstance(res[i], ValueError))
            else:
                self.assertEqual(i, res[i])

        # error re-raised
        try:
            res = parallel_exec([(i, err, [i]) for i in range(0, 5)], raise_errors=True)
            self.fail("Error should have been raised")
        except ValueError as ve:
            LOG.info("ve=%s", ve)

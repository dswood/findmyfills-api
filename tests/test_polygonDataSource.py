#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import datetime
import json
import logging
import os
import unittest
from collections import Counter, defaultdict
from unittest import TestCase

import pandas as pd

from logging_helper import init_logging, log_list
from marketdata.polygon import PolygonDataSource, unmap_json_fields, AGG_FIELD_MAP
from util import ms_to_datetime64, datetime_to_millis

init_logging(level_name='DEBUG')
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 2000)

LOG = logging.getLogger("TestPolygonDataSource")


def connects_to_polygon_io(fn):
    """
    Decorator to indicate a test method will make a real connection to polygon.io.
    Requires a polygon key and network connectivity
    """

    def wrapper(x):
        fn(x)

    return wrapper


class TestPolygonDataSource(TestCase):

    def setUp(self):
        test_cache_dir = os.path.join(os.path.dirname(__file__), 'test_data')
        self.ds = PolygonDataSource(cache_dir=test_cache_dir)

    def test_init(self):
        # quick sanity check some values that are cached at start (note int keys)
        self.assertTrue(len(self.ds.trade_cond_map.keys()) > 0)
        self.assertEqual(self.ds.trade_cond_map[0], 'Regular')
        self.assertEqual(self.ds.trade_cond_map[37], 'OddLot')

        self.assertTrue(len(self.ds.exchange_map.keys()) > 0)
        self.assertEqual(self.ds.exchange_map[10], 'XNYS')
        self.assertEqual(self.ds.exchange_map[15], 'IEXG')

    def test_mic_list(self):
        """ Generate a reasonable MIC list in json for front-end """
        ignore_list = [0, 5, 6, 13, 14, 141, 142, 143, 144, 144, 145, 16, 33]
        exch_list = []
        for exch in self.ds.raw_exchanges:
            exch_id = exch['id']
            if exch_id in ignore_list:
                continue
            name = exch['name']
            mic = self.ds.exchange_map.get(exch_id)
            if mic:
                LOG.info("%d=%s (%s)", exch_id, mic, name)
                exch_list.append({"mic": mic, "name": name})
        LOG.info("\n%s", json.dumps(exch_list, indent=4))

    def test_trade_parse(self):
        """
        basic trade parse test from static data
        """
        # {'c1': 17, 'c2': 9, 'c3': 41, 'c4': 0, 'e': '15', 'p': 54.43, 's': 262, 't': 1560519000001}
        raw_trades = self.ds.load_json('trades.json')
        # {'symbol': 'IBKR', 'exchange': 'IEXG', 'price': 54.43, 'size': 262,
        #  'timestamp': datetime.datetime(2019, 6, 14, 9, 30, 0, 1000),
        #  'conds': ['MarketCenterOpening', 'Cross', 'TradeThruExempt', 'Regular']}
        trades = unmap_json_fields(raw_trades, self.ds.format_trade)
        log_list(LOG, trades, 5)
        self.assertEqual(trades[0]['exchange'], 'IEXG')
        self.assertEqual(trades[0]['conds'], ['MarketCenterOpening', 'Cross', 'TradeThruExempt', 'Regular'])

    def test_trade_v2_parse(self):
        """
        basic trade parse test from static data (v2 of api)
        :return:
        """
        raw_trades = self.ds.load_json('trades_v2.json')
        trades = unmap_json_fields(raw_trades, self.ds.format_trade_v2)
        log_list(LOG, trades, 10)
        trade = trades[0]
        self.assertEqual(trade['exchange'], 'XNGS')
        self.assertEqual(trade['price'], 41.62)
        self.assertEqual(trade['size'], 2226)
        self.assertEqual(trade['conds'], ['Cross', 'MarketCenterOpening'])
        self.assertEqual(trade['flags'], 'pcv')
        trade = trades[1]
        self.assertEqual(trade['exchange'], 'XNGS')
        self.assertEqual(trade['price'], 41.62)
        self.assertEqual(trade['size'], 2226)
        self.assertEqual(trade['conds'], ['MarketCenterOfficialOpen'])
        self.assertEqual(trade['flags'], '')

    def test_nbbo_parse(self):
        """
        basic quote parse test from static data
        """
        raw_data = self.ds.load_json('nbbo.json')
        nbbos = unmap_json_fields(raw_data, self.ds.format_nbbo)
        log_list(LOG, nbbos, 10)
        nbbo = nbbos[0]
        self.assertEqual(nbbo['ask_price'], 41.8)
        self.assertEqual(nbbo['ask_size'], 300)
        self.assertEqual(nbbo['ask_exchange'], 'ARCX')
        self.assertEqual(nbbo['bid_price'], 41.63)
        self.assertEqual(nbbo['bid_size'], 200)
        self.assertEqual(nbbo['bid_exchange'], 'XNGS')

    def test_nbbo_parse_non_standard_lot_size(self):
        """
        basic quote parse test from static data
        """
        raw_data = self.ds.load_json('nbbo_BRK.A.json')
        nbbos = unmap_json_fields(raw_data, self.ds.format_nbbo)
        log_list(LOG, nbbos, 10)
        nbbo = nbbos[0]
        self.assertEqual(nbbo['ask_price'], 322482.77)
        self.assertEqual(nbbo['ask_size'], 1)
        self.assertEqual(nbbo['ask_exchange'], 'XNYS')
        self.assertEqual(nbbo['bid_price'], 320578.35)
        self.assertEqual(nbbo['bid_size'], 1)
        self.assertEqual(nbbo['bid_exchange'], 'BATS')

    def test_quote_parse(self):
        """
        basic quote parse test from static data
        """
        raw_quotes = self.ds.load_json('quotes.json')
        quotes = unmap_json_fields(raw_quotes, self.ds.format_quote)
        log_list(LOG, quotes, 10)
        self.assertEqual(quotes[0]['bidexchange'], 'ARCX')
        self.assertEqual(quotes[0]['askexchange'], 'ARCX')
        self.assertEqual(quotes[0]['bidprice'], None)
        self.assertEqual(quotes[0]['bidsize'], None)
        self.assertEqual(quotes[0]['askprice'], 60.09)
        self.assertEqual(quotes[0]['asksize'], 1)
        self.assertEqual(quotes[0]['condition'], 'Regular')

    def test_bars_parse(self):
        """
        parse bar data from polygon
        """
        raw_data = self.ds.load_json('bars_1_minute.json')
        LOG.info(raw_data)
        bars = unmap_json_fields(raw_data, self.ds.format_bar, AGG_FIELD_MAP)
        log_list(LOG, bars, 5)
        bar = bars[1]
        self.assertEqual(bar['open'], 30.43)
        self.assertEqual(bar['close'], 30.64)
        self.assertEqual(bar['high'], 30.71)
        self.assertEqual(bar['low'], 30.43)
        self.assertEqual(bar['volume'], 2578)
        # N.B. - might change in future after backfill per polygon support
        self.assertEqual(bar['count'], 1)

    @unittest.skip
    def test_gen_flags_test_from_trades(self):
        """
        helper to generate test cases based on each listing markets trades
        """
        test_cases = []
        conds_all = set()
        for sym in ['IBKR', 'LOV', 'FOX', 'XLB', 'IBM', 'CBOE']:
            raw_trades = self.ds.load_json('trades_fullday_%s.json.gz' % sym)
            trades = unmap_json_fields(raw_trades, self.ds.format_trade)
            LOG.info("[%s] trades: %d" % (sym, len(trades)))

            conds = dict()
            i = 0
            for trade in trades:
                i += 1
                a = "', '".join(trade['conds'])
                n = conds.get(a, 0)
                conds[a] = n + 1

            test_cases.append("\n        # %s" % sym)
            for k, v in conds.items():
                fc = "# " if k in conds_all else ""
                c = "dupe" if k in conds_all else ""
                tc = "        %sself.sale_cond_test('%s', 1, 1, ['%s'])" % (fc, c, k)
                test_cases.append(tc)
                conds_all.add(k)
                # LOG.info("['%s'] = %s" % (k, v))
        LOG.info("\n" + "\n".join(test_cases))

    def test_is_last_sale_eligible(self):
        """
        Based on a days trades from each listing market, verify last sale and volume eligibility
        """
        # IBKR full day (IEXG primary)
        self.sale_cond_test('iex open', 1, 1, ['MarketCenterOpening', 'Cross', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('odd lot', 0, 1, ['OddLot', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('iso', 1, 1, ['InterMarketSweep', 'TradeThruExempt', 'Regular', 'Regular'])
        self.sale_cond_test('iso oddlot', 0, 1, ['InterMarketSweep', 'OddLot', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('!prim open', 0, 0, ['MarketCenterOfficialOpen', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('normal', 1, 1, ['Regular', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('!prim close', 0, 0, ['MarketCenterOfficialClose', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('prim close', 1, 1, ['MarketCenterClosing', 'Cross', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('prim restate', 1, 0,
                            ['CorrectedConsolidatedClosePrice', 'Regular', 'Regular', 'Regular'])
        # LOV
        # self.sale_cond_test('dupe', 1, 1, ['Regular', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOfficialOpen', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['OddLot', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'TradeThruExempt', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOfficialClose', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('prim restate2', 1, 0,
                            ['CorrectedConsolidatedClosePrice', 'TradeThruExempt', 'Regular', 'Regular'])

        # FOX
        self.sale_cond_test('ft,cash,oddlot', 0, 1, ['CashTrade', 'FormT(ExtendedHours)', 'OddLot', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOpening', 'Cross', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOfficialOpen', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['OddLot', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('dp,oddlot', 0, 1, ['DerivativelyPriced', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['Regular', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'TradeThruExempt', 'Regular', 'Regular'])
        self.sale_cond_test('dp,sos', 0, 1,
                            ['DerivativelyPriced', 'SoldOutOfSequence', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'OddLot', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('tte?', 1, 1, ['TradeThruExempt', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('oddlot', 0, 1, ['OddLot', 'TradeThruExempt', 'Regular', 'Regular'])
        self.sale_cond_test('avgPx', 0, 1,
                            ['DerivativelyPriced', 'AveragePrice', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('contigent', 0, 1,
                            ['Contingent(Qualified)', 'Contingent', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOfficialClose', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterClosing', 'Cross', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('T', 0, 1, ['FormT(ExtendedHours)', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('T,oddlot', 0, 1, ['FormT(ExtendedHours)', 'OddLot', 'Regular', 'Regular'])
        self.sale_cond_test('T,avgPx', 0, 1, ['FormT(ExtendedHours)', 'AveragePrice', 'Regular', 'Regular'])
        self.sale_cond_test('nextday,T', 0, 1, ['NextDay', 'FormT(ExtendedHours)', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('ISO,T,oddlot', 0, 1,
                            ['InterMarketSweep', 'FormT(ExtendedHours)', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('ISO,T,oddlot', 0, 1,
                            ['InterMarketSweep', 'FormT(ExtendedHours)', 'OddLot', 'TradeThruExempt'])

        # XLB
        self.sale_cond_test('arca open', 1, 1, ['MarketCenterOpening', 'TradeThruExempt', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOfficialOpen', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'TradeThruExempt', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['OddLot', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['Regular', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['DerivativelyPriced', 'AveragePrice', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['DerivativelyPriced', 'OddLot', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('avg px2', 0, 1, ['AveragePrice', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['Contingent(Qualified)', 'Contingent', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['OddLot', 'TradeThruExempt', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['TradeThruExempt', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('cash,oddlot', 0, 1, ['CashTrade', 'OddLot', 'Regular', 'Regular'])
        self.sale_cond_test('cont,avgpx', 0, 1,
                            ['Contingent(Qualified)', 'AveragePrice', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('nextday,oddlot', 0, 1, ['NextDay', 'OddLot', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('arca close', 1, 1, ['MarketCenterClosing', 'TradeThruExempt', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOfficialClose', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['FormT(ExtendedHours)', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['FormT(ExtendedHours)', 'OddLot', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1,
        #                     ['CorrectedConsolidatedClosePrice', 'TradeThruExempt', 'Regular', 'Regular'])

        # IBM
        # self.sale_cond_test('dupe', 1, 1, ['FormT(ExtendedHours)', 'OddLot', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'FormT(ExtendedHours)', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'FormT(ExtendedHours)', 'OddLot', 'TradeThruExempt'])
        # self.sale_cond_test('dupe', 1, 1, ['FormT(ExtendedHours)', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['CashTrade', 'FormT(ExtendedHours)', 'OddLot', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['OddLot', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOfficialOpen', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('nyse open', 1, 1, ['MarketCenterOpening', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['Regular', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['DerivativelyPriced', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'TradeThruExempt', 'Regular', 'Regular'])
        self.sale_cond_test('cash,oddlot', 0, 1, ['CashTrade', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['DerivativelyPriced', 'AveragePrice', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('avgpx nyse', 0, 1, ['AveragePrice', 'TradeThruExempt', 'Regular', 'Regular'])
        self.sale_cond_test('cont,oddlot', 0, 1, ['Contingent(Qualified)', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['Contingent(Qualified)', 'AveragePrice', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('cash,sos,odd', 0, 1, ['CashTrade', 'SoldOutOfSequence', 'OddLot', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['Contingent(Qualified)', 'Contingent', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['NextDay', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['CashTrade', 'OddLot', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['TradeThruExempt', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('cancel', 0, 0, ['Cancelled', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['OddLot', 'TradeThruExempt', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOfficialClose', 'Regular', 'Regular', 'Regular'])
        self.sale_cond_test('nsye close', 1, 1, ['MarketCenterClosing', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['FormT(ExtendedHours)', 'AveragePrice', 'Regular', 'Regular'])
        self.sale_cond_test('T,oddlot', 0, 1, ['FormT(ExtendedHours)', 'OddLot', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('T,prpx', 0, 1,
                            ['FormT(ExtendedHours)', 'PriorReferencePrice', 'Regular', 'Regular'])
        self.sale_cond_test('Tsos,oddlog', 0, 1, ['FormTOutOfSequence', 'OddLot', 'Regular', 'Regular'])

        # CBOE
        # self.sale_cond_test('dupe', 1, 1, ['FormT(ExtendedHours)', 'OddLot', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOpening', 'Cross', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOfficialOpen', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['OddLot', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['InterMarketSweep', 'TradeThruExempt', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['Regular', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['DerivativelyPriced', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['DerivativelyPriced', 'AveragePrice', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['CashTrade', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterClosing', 'Cross', 'TradeThruExempt', 'Regular'])
        self.sale_cond_test('close', 0, 0,
                            ['MarketCenterOfficialClose', 'TradeThruExempt', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['MarketCenterOfficialClose', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['FormT(ExtendedHours)', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['FormT(ExtendedHours)', 'OddLot', 'TradeThruExempt', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['CorrectedConsolidatedClosePrice', 'Regular', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['FormT(ExtendedHours)', 'AveragePrice', 'Regular', 'Regular'])
        self.sale_cond_test('Next,T,oddlot', 0, 1, ['NextDay', 'FormT(ExtendedHours)', 'OddLot', 'TradeThruExempt'])
        # self.sale_cond_test('dupe', 1, 1, ['FormT(ExtendedHours)', 'PriorReferencePrice', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['FormTOutOfSequence', 'OddLot', 'Regular', 'Regular'])
        # self.sale_cond_test('dupe', 1, 1, ['NextDay', 'FormT(ExtendedHours)', 'TradeThruExempt', 'Regular'])

    def sale_cond_test(self, desc, expected_lse, expected_ve, conds):
        b_expected_lse = True if expected_lse == 1 else False
        b_expected_ve = True if expected_ve == 1 else False
        LOG.info("%15s (lse=%5s, ve=%5s) %s" % (desc, b_expected_lse, b_expected_ve, conds))

        lse = self.ds.is_last_sale_eligible(conds)
        self.assertEqual(b_expected_lse, lse, "LSE:" + desc)

        ve = self.ds.is_volume_eligible(conds)
        self.assertEqual(b_expected_ve, ve, "VE:" + desc)

    def test_chartable(self):
        self.chartable_cond_test("OddLot", True, ['OddLot'])
        self.chartable_cond_test("FormT", True, ['FormT(ExtendedHours)'])

        # neg cases
        self.chartable_cond_test("NextDay", False, ['NextDay'])

    def chartable_cond_test(self, desc, expected_chartable, conds):
        LOG.info("%15s chartable=%s for %s" % (desc, expected_chartable, conds))

        chartable = self.ds.is_chartable(conds)
        self.assertEqual(expected_chartable, chartable, "Chartable:" + desc)

    def test_is_block_trade(self):
        self.run_block_trade_test(1.00, 100, False)
        self.run_block_trade_test(1.00, 10000, True)
        self.run_block_trade_test(1.00, 200000, True)
        self.run_block_trade_test(0.60, 2000000, True)

    def run_block_trade_test(self, price, size, is_block):
        msg = "is_block_trade(%d @ $%f ($%d)) -> %s" % (size, price, price * size, is_block)
        LOG.info(msg)
        self.assertEqual(is_block, self.ds.is_block_trade({'price': price, 'size': size}), msg=msg)

    def load_trades_fullday(self, sym):
        raw_trades = self.ds.load_json('trades_fullday_%s.json.gz' % sym)
        trades = unmap_json_fields(raw_trades, self.ds.format_trade)
        LOG.info("%s: loaded trades(%d)" % (sym, len(trades)))
        return trades

    @connects_to_polygon_io
    def test_calc_bars_IBKR(self):
        # N.B. at 14th bar things mismatch on the Polygon side
        self.run_calc_bars_compare('IBKR', 10, fail=False, verbose=False)

    @connects_to_polygon_io
    def test_calc_bars_LOV(self):
        # LOV: immediately fails in first bin...6/50 fail
        self.run_calc_bars_compare('LOV', 50, fail=False, verbose=False)

    @connects_to_polygon_io
    def test_calc_bars_IBM(self):
        self.run_calc_bars_compare('IBM', 10, fail=False, verbose=False)

    @connects_to_polygon_io
    def test_calc_bars_XLB(self):
        # also double counting official open
        self.run_calc_bars_compare('XLB', 10, fail=False, verbose=False)

    @connects_to_polygon_io
    def test_calc_bars_FOX(self):
        # 10 of 10 don't match...starting bins cuz official open counted...others, not sure
        self.run_calc_bars_compare('FOX', 10, fail=False, verbose=False)

    @connects_to_polygon_io
    def test_calc_bars_CBOE(self):
        self.run_calc_bars_compare('CBOE', 10, fail=False, verbose=False)

    def run_calc_bars_compare(self, sym, bar_limit, fail=True, verbose=False):
        """
        Compare bars calcuated from trades vs polygon
        There are differences so just log differences
        """
        start = datetime.datetime.strptime('2019-06-14 09:30:00 -0400', '%Y-%m-%d %H:%M:%S %z')
        end = start + datetime.timedelta(minutes=20)

        # get trades and calc 1 minute bars
        trades = self.ds.get_trades_v2(sym, start, end, 50000)
        for trade in trades:
            trade['timestamp'] = trade['sip_timestamp']
        df_bars_calc = self.ds.calc_bars(trades, groupby_freq='60s', time_field='timestamp')
        df_bars_calc.rename(columns={'sip_timestamp': 'timestamp'})
        LOG.info("\n%s" % df_bars_calc.head(bar_limit))

        # call poloygon and get 1 minute bars
        df_bars_pg = pd.DataFrame(self.ds.get_bars(sym, start, start + datetime.timedelta(days=1)))

        # check first couple of bars
        del df_bars_pg['symbol']
        df_bars_pg = df_bars_pg[['timestamp', 'count', 'open', 'close', 'high', 'low', 'volume']]
        df_bars_pg = df_bars_pg[df_bars_pg['timestamp'] >= datetime_to_millis(start) ]
        ms_to_datetime64(df_bars_pg, 'timestamp')
        df_bars_pg.rename(columns={'count': 'n'}, inplace=True)
        LOG.info("\n%s" % df_bars_pg.head(bar_limit))

        all_mismatches = []
        for i in range(len(df_bars_calc) - 1):
            mismatches = self.verifyBar(df_bars_pg.iloc[i], df_bars_calc.iloc[i])
            if len(mismatches) > 0:
                all_mismatches.extend(mismatches)
        LOG.info("mismatched bins=%d", len(all_mismatches))

        df_trades = pd.DataFrame(trades)
        ms_to_datetime64(df_trades, 'timestamp')
        for mismatch in all_mismatches:
            start = mismatch['timestamp']
            end = start + datetime.timedelta(minutes=1)
            mask = (df_trades['timestamp'] >= start) & (df_trades['timestamp'] < end)
            msg = "%s\n%s\n" % (mismatch, df_trades.loc[mask])
            if fail:
                self.fail(msg)
            elif verbose:
                LOG.warning(msg)
            else:
                LOG.warning(mismatch)
        if len(all_mismatches) > 0:
            LOG.warning("[%s] %d of %d bars mismatched" % (sym, len(all_mismatches), len(df_bars_pg)))

    def test_get_bars_subminute(self):
        """
        verify calculating bars
        """
        trades = self.load_trades_fullday('IBM')
        df = self.ds.calc_bars(trades, groupby_freq='1s')
        LOG.info("\n%s" % df.head(10))
        LOG.info("\n%s" % trades)

        start = datetime_to_millis(datetime.datetime.strptime('2019-06-14 09:31:09 -0400', '%Y-%m-%d %H:%M:%S %z'))
        end = datetime_to_millis(datetime.datetime.strptime('2019-06-14 09:31:10 -0400', '%Y-%m-%d %H:%M:%S %z'))
        data_1s = list(filter(lambda x: start <= x['timestamp'] < end, trades))
        df_1s = pd.DataFrame(data_1s)
        ms_to_datetime64(df_1s, 'timestamp')
        LOG.info("\n%s", df_1s)

        sz = list(map(lambda x: x['size'], filter(lambda x: 'v' in x['flags'], data_1s)))
        lse = list(map(lambda x: x['price'], filter(lambda x: 'p' in x['flags'], data_1s)))

        # compare some fields
        row = df.iloc[2]
        self.assertEqual(len(data_1s), row['n'])
        self.assertEqual(sum(sz), row['volume'])
        self.assertEqual(lse[0], row['open'])
        self.assertEqual(lse[-1], row['close'])
        self.assertEqual(max(lse), row['high'])
        self.assertEqual(min(lse), row['low'])

    @staticmethod
    def verifyBar(bar_expected, bar_calc):
        # N.B. - count is incorrect on polygon side...exclude for now
        mismatches = []
        for fn in ['timestamp', 'open', 'close', 'high', 'low', 'volume']:
            v1 = bar_expected.loc[fn]
            v2 = bar_calc.loc[fn]
            if v1 != v2:
                mismatches.append({
                    'timestamp': bar_expected.loc['timestamp'],
                    'field': fn,
                    'expected': v1,
                    'calc': v2})
        return mismatches

    @connects_to_polygon_io
    def test_trade_v2(self):
        """V2 trades...check, start and end time"""
        sym = 'IBKR'
        date = '2019-07-05 09:45:00 -0400'
        dt_start = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S %z')
        dt_end = dt_start + datetime.timedelta(minutes=10)
        trades = self.ds.get_trades_v2(sym, dt_start, dt_end, 1000, True)
        first_trade = trades[0]
        last_trade = trades[len(trades) - 1]
        LOG.info("first=%s" % first_trade)
        LOG.info(" last=%s" % last_trade)
        self.assertTrue(first_trade['sip_timestamp'] > dt_start.timestamp() * 1000)
        self.assertTrue(last_trade['sip_timestamp'] < dt_end.timestamp() * 1000)

    def test_localize_time_column(self):
        """
        workaround to localize a column.  slow so will not use but left for reference
        """
        trades = self.load_trades_fullday('IBM')
        df_trades = pd.DataFrame(trades)
        ms_to_datetime64(df_trades, 'timestamp')

        # do group by...timestamp will have no tz attached
        bars = self.ds.calc_bars(trades[:200], groupby_freq="1s", time_field='timestamp')
        ts_series = bars['timestamp']
        first_ts = ts_series[0]
        LOG.info("BARS before, timestamp=%s (tzinfo=%s)\n%s" % (first_ts, first_ts.tzinfo, bars))
        self.assertEquals(None, first_ts.tzinfo)

        # work around based on patch seen online...WARNING: heavy call
        # (https://github.com/pandas-dev/pandas/commit/64a84cac5dc88a34466dec09cb1384cb327875b2)
        ts_series = ts_series.dt.tz_localize('utc')
        ts_series = ts_series.dt.tz_convert('US/Eastern')
        bars['timestamp'] = ts_series
        first_ts = ts_series[0]
        LOG.info("BARS after, timestamp=%s (tzinfo=%s)\n%s" % (first_ts, first_ts.tzinfo, bars))
        self.assertEqual('US/Eastern', first_ts.tzinfo.zone)

    def test_summarize_trades(self):
        trades = self.load_trades_fullday('IBM')
        df_agg = self.ds.calc_venue_summary(trades)

        LOG.info("\n%s", df_agg)

        # manually to verify some of the columns
        c_all = Counter(list(map(lambda x: x['exchange'], trades)))
        v_lse = defaultdict(int)
        notional = defaultdict(int)
        for t in filter(lambda x: "p" in x['flags'], trades):
            v_lse[t['exchange']] += t['size']
            notional[t['exchange']] += t['size'] * t['price']

        for index, row in df_agg.iterrows():
            ex = row['exchange']
            if ex == 'TOTAL':
                continue

            self.assertEqual(c_all[ex], row['n'])
            vol_lse = v_lse[ex]
            self.assertEqual(vol_lse, row['v_lse'])
            if vol_lse > 0:
                self.assertAlmostEqual(notional[ex] / vol_lse, row['px_vwap_lse'], 4)

    @connects_to_polygon_io
    def test_get_nbbo_asof(self):
        """
        Exercise nbbo-asof api from polygon
        """

        # get nbbos
        time = datetime.datetime.strptime('2019-06-14 10:04:00 -0400', '%Y-%m-%d %H:%M:%S %z')
        delta = datetime.timedelta(minutes=2)
        nbbos = self.ds.get_nbbo('IBKR', time - delta, time + delta, limit=1000, convert=True)
        df = pd.DataFrame(nbbos)
        df_before = df[df['sip_timestamp'] < datetime_to_millis(time)]
        asof_nbbo_manual = df_before.tail(1).to_dict(orient='records')[0]
        ms_to_datetime64(df_before, 'sip_timestamp')
        LOG.info("\n%s", df_before.tail(10))

        # get asof
        asof_nbbo = self.ds.get_nbbo_asof('IBKR', time, True)
        # translate_to_datetime64([asof_nbbo], 'sip_timestamp')
        LOG.info("asof_nbbo1=%s", asof_nbbo)
        LOG.info("asof_nbbo2=%s", asof_nbbo_manual)
        for k, v_expected in asof_nbbo_manual.items():
            v = asof_nbbo[k]
            self.assertEqual(v_expected, v)

    @unittest.skip("requests a large amount of data")
    @connects_to_polygon_io
    def test_get_trades_v2_chunking(self):
        """
        get trades_v2 with paging...so results > 50k
        """
        dt_start = datetime.datetime.strptime('2019-07-05 09:30:00', '%Y-%m-%d %H:%M:%S')
        dt_end = datetime.datetime.strptime('2019-07-05 16:30:00', '%Y-%m-%d %H:%M:%S')
        trades = self.ds.get_trades_v2('SPY', dt_start, dt_end, 50_001)
        self.ds.save_json("trades_paging_50001.json", trades)
        LOG.info("trades=%d", len(trades))

        df_trades = pd.DataFrame(trades[-10:])
        ms_to_datetime64(df_trades, 'sip_timestamp')
        LOG.info("\n%s", df_trades)

    @unittest.skip("requests a large amount of data")
    @connects_to_polygon_io
    def test_get_nbbo_chunking(self):
        """
        get nbbo with paging...so results > 50k
        """
        dt_start = datetime.datetime.strptime('2019-07-05 09:30:00', '%Y-%m-%d %H:%M:%S')
        dt_end = datetime.datetime.strptime('2019-07-05 16:30:00', '%Y-%m-%d %H:%M:%S')
        nbbos = self.ds.get_nbbo('SPY', dt_start, dt_end, 50_001, True)
        self.ds.save_json("nbbo_paging_50001.json", nbbos)
        LOG.info("nbbos=%d", len(nbbos))

        df = pd.DataFrame(nbbos[-10:])
        ms_to_datetime64(df, 'sip_timestamp')
        LOG.info("\n%s", df)

#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import datetime
import logging
import os
from unittest import TestCase

from logging_helper import init_logging
from marketdata.cache.postgres_cache import PostgresCache
from util import datetime_to_millis

init_logging()
LOG = logging.getLogger(__name__)

# get connection string from environment: (e.g. postgresql+psycopg2://USER:PASSWORD@127.0.0.1:5432/FMF)
TEST_CONNECTION_STRING = os.environ["FMF_POSTGRES_CONNECTION_STRING"]
TEST_TABLE = 'TEST2'
TEST_JSON_1 = "{[ {'a':1, 'b':2} ]}"
TEST_JSON_2 = "{[ {'c':1, 'd':2} ]}"


def dt(time_str):
    d = datetime.datetime.strptime('2019-08-05 ' + time_str, '%Y-%m-%d %H:%M:%S.%f')
    LOG.debug(f"{d}={datetime_to_millis(d)}")
    return datetime_to_millis(d)


class TestPostgresCache(TestCase):
    """
    To run:
    - need to have environment variable with connection string
    - running cloudproxy in gcloud shell (e.g. logged into project, running proxy)
      (e.g. cloud_sql_proxy_x64.exe -instances=findmyfills-api:us-central1:test=tcp:5432  )
      More info: https://cloud.google.com/sql/docs/postgres/connect-external-app
    """
    def setUp(self):
        self.pc = PostgresCache(TEST_CONNECTION_STRING)
        self.pc.drop_table(TEST_TABLE)
        self.assertFalse(self.pc.has_table(TEST_TABLE))

    def test_get_table_names(self):
        LOG.info("table_names=%s", self.pc.get_table_names())

    def test_get_table(self):
        self.assertFalse(TEST_TABLE in self.pc.get_table_names())

        # creates table
        table1 = self.pc.get_table(TEST_TABLE)
        self.assertTrue(TEST_TABLE in self.pc.get_table_names())

        # get cached
        table2 = self.pc.get_table(TEST_TABLE)
        self.assertTrue(table1 == table2)

    def test_get_cache_exact(self):
        self.run_single_get_test(dt('09:30:00.000'), dt('09:40:00.000'))

    def test_get_cache_subset(self):
        self.run_single_get_test(dt('09:35:00.000'), dt('09:38:00.000'))

    def test_get_cache_overlap_start(self):
        self.run_single_get_test(dt('09:20:00.000'), dt('09:31:00.000'))

    def test_get_cache_overlap_end(self):
        self.run_single_get_test(dt('09:36:00.000'), dt('09:50:00.000'))

    def test_get_cache_not_in_range_start(self):
        self.run_single_get_test(dt('09:10:00.000'), dt('09:20:00.000'), False)

    def test_get_cache_not_in_range_end(self):
        self.run_single_get_test(dt('09:45:00.000'), dt('09:50:00.000'), False)

    def run_single_get_test(self, query_start_ms, query_end_ms, expect=True):
        start_ms = dt('09:30:00.000')
        end_ms = dt('09:40:00.000')
        self.pc.set_cache(TEST_TABLE, 'A', start_ms, end_ms, TEST_JSON_1)
        data = self.pc.get_cache(TEST_TABLE, 'A', query_start_ms, query_end_ms)
        LOG.info(data)
        if expect:
            self.assertEqual(1, len(data))
            item = data[(start_ms, end_ms)]
            self.assertEqual(TEST_JSON_1, item)
        else:
            self.assertEqual(0, len(data))

    def test_get_cache_multi(self):
        s1_ms = dt('09:30:00.000')
        e1_ms = dt('09:35:00.000')
        s2_ms = dt('09:35:00.000')
        e2_ms = dt('09:40:00.000')
        s3_ms = dt('09:32:00.000')
        e3_ms = dt('09:50:00.000')
        self.pc.set_cache(TEST_TABLE, 'A', s1_ms, e1_ms, TEST_JSON_1)
        self.pc.set_cache(TEST_TABLE, 'A', s2_ms, e2_ms, TEST_JSON_2)
        self.pc.set_cache(TEST_TABLE, 'A', s3_ms, e3_ms, TEST_JSON_1)
        data = self.pc.get_cache(TEST_TABLE, 'A', dt('09:30:00.000'), dt('16:00:00.000'))
        LOG.info(data)
        self.assertEqual(3, len(data))
        json1 = data[(s1_ms, e1_ms)]
        json2 = data[(s2_ms, e2_ms)]
        json3 = data[(s3_ms, e3_ms)]
        self.assertEqual(TEST_JSON_1, json1)
        self.assertEqual(TEST_JSON_2, json2)
        self.assertEqual(TEST_JSON_1, json3)

    def test_get_cache_bc(self):
        # smallest data point is 1 ms
        s1_ms = dt('09:30:00.000')
        e1_ms = dt('09:30:00.001')
        self.pc.set_cache(TEST_TABLE, 'A', s1_ms, e1_ms, TEST_JSON_1)
        data = self.pc.get_cache(TEST_TABLE, 'A', dt('09:30:00.000'), dt('16:00:00.000'))
        LOG.info(data)
        self.assertEqual(1, len(data))
        self.assertEqual(TEST_JSON_1, data[(s1_ms, e1_ms)])

        data = self.pc.get_cache(TEST_TABLE, 'A', dt('09:20:00.000'), dt('09:30:00.000'))
        LOG.info(data)
        self.assertEqual(0, len(data))

        data = self.pc.get_cache(TEST_TABLE, 'A', dt('09:30:00.001'), dt('09:31:00.000'))
        LOG.info(data)
        self.assertEqual(0, len(data))

    def test_get_cache_sym_diff(self):
        # store 2 symbols
        s1_ms = dt('09:30:00.000')
        e1_ms = dt('09:40:00.000')
        self.pc.set_cache(TEST_TABLE, 'A', s1_ms, e1_ms, TEST_JSON_1)
        self.pc.set_cache(TEST_TABLE, 'B', s1_ms, e1_ms, TEST_JSON_2)
        data1 = self.pc.get_cache(TEST_TABLE, 'A', dt('09:30:00.000'), dt('16:00:00.000'))
        data2 = self.pc.get_cache(TEST_TABLE, 'B', dt('09:30:00.000'), dt('16:00:00.000'))
        LOG.info(f"A={data1}, B={data2}")
        self.assertEqual(1, len(data1))
        self.assertEqual(TEST_JSON_1, data1[(s1_ms, e1_ms)])
        self.assertEqual(1, len(data2))
        self.assertEqual(TEST_JSON_2, data2[(s1_ms, e1_ms)])

        # delete B
        self.pc.del_cache(TEST_TABLE, 'B', s1_ms)
        data2 = self.pc.get_cache(TEST_TABLE, 'B', dt('09:30:00.000'), dt('16:00:00.000'))
        LOG.info(f"B={data2}")
        self.assertEqual(0, len(data2))

    def test_set_then_del_cache(self):
        # stick in 1 row and pull back out
        start_ms = dt('09:30:00.000')
        end_ms = dt('09:31:00.000')
        self.pc.set_cache(TEST_TABLE, 'A', start_ms, end_ms, TEST_JSON_1)
        rows = self.pc.get_all_rows(self.pc.get_table(TEST_TABLE))
        LOG.info(f"all_rows={rows}")
        self.assertTrue(len(rows) == 1)
        row = rows[0]
        self.assertEqual('A', row['symbol'])
        self.assertEqual(start_ms, datetime_to_millis(row['start_time']))
        self.assertEqual(end_ms, datetime_to_millis(row['end_time']))
        self.assertEqual(TEST_JSON_1, row['json_data'])

        # delete it now
        self.pc.del_cache(TEST_TABLE, 'A', start_ms)
        rows = self.pc.get_all_rows(self.pc.get_table(TEST_TABLE))
        self.assertTrue(len(rows) == 0)

    def test_list_rows(self):
        self.pc.list_table_rows("test list", self.pc.get_table(TEST_TABLE))

    def test_del_cache(self):
        start_ms = dt('09:30:00.000')
        end_ms = dt('09:31:00.000')
        self.pc.set_cache(TEST_TABLE, 'A', start_ms, end_ms, TEST_JSON_1)

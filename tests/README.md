
## Note ##

- test_data directory contains data used in testing
- test_data directory is also used as scratch directory for some tests
- static files that contain price have had prices adjusted by a small, random amount.  They are NOT accurate.
- many tests also connect to polygon to retrieve data
  - these tests are decorated with @connects_to_polygon_io 
  - to make these run, a valid polygon api key should be in the enviroment variable ```POLYGON_API_KEY```

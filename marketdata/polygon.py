#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#

import datetime
import gzip
import os
import tempfile
from enum import Enum, auto

import pandas as pd
import requests
import json
import logging

from util import ms_to_datetime64, parallel_exec

LOG = logging.getLogger(os.path.basename(__file__))

POLYGON_API_KEY = os.environ['POLYGON_API_KEY']
POLYGON_QUERY_LIMIT = 50_000
POLYGON_QUERY_TIMEOUT = os.environ.get('POLYGON_QUERY_TIMEOUT', 10)
AGG_FIELD_MAP = {
    'o': 'open',
    'c': 'close',
    'h': 'high',
    'l': 'low',
    'v': 'volume',
    'n': 'count',
    't': 'timestamp'
}

NON_LAST_SALE_ELIGIBLE_CONDS = {
    'CashTrade',
    'BunchSold'
    'PriceVariation',
    'OddLot',
    'MarketCenterOfficialClose',
    'NextDay',
    'PriorReferencePrice',       # can be if only trade (note #2)
    'MarketCenterOfficialOpen',  # can be last if only if first trade
    'Seller',
    'FormT(ExtendedHours)',
    'FormTOutOfSequence',
    'Contingent',
    'AveragePrice',
    'SoldOutOfSequence',         # can be last if only trade in the market
    'DerivativelyPriced',        # can be last if only trade in the market
    'Contingent(Qualified)',
    'Cancelled'
}
NON_CHARTABLE_ELIGIBLE_CONDS = {
    'CashTrade',
    'BunchSold'
    'PriceVariation',
    # 'OddLot',
    'MarketCenterOfficialClose',
    'NextDay',
    'PriorReferencePrice',       # can be if only trade (note #2)
    'MarketCenterOfficialOpen',  # can be last if only if first trade
    'Seller',
    # 'FormT(ExtendedHours)',
    'FormTOutOfSequence',
    'Contingent',
    'AveragePrice',
    'SoldOutOfSequence',         # can be last if only trade in the market
    'DerivativelyPriced',        # can be last if only trade in the market
    'Contingent(Qualified)',
    'Cancelled'
}
NON_VOLUME_ELIGIBLE_CONDS = {
    'MarketCenterOfficialClose',
    'MarketCenterOfficialOpen',
    'CorrectedConsolidatedClosePrice',
    'Cancelled'
}

# generated from get_lot_sizes.py
NON_STANDARD_LOT_SIZES = {
    'BAC$L': 10,  # (N) Bank of America Corporation Non Cumulative Perpetual Conv Pfd Ser L
    'BH': 10,  # (N) Biglari Holdings Inc. Class B Common Stock
    'BH.A': 10,  # (N) Biglari Holdings Inc. Class A Common Stock
    'BRK.A': 1,  # (N) Berkshire Hathaway Inc. Common Stock
    'CMS$B': 10,  # (N) CMS Energy Corporation Preferred Stock
    'DIT': 10,  # (A) AMCON Distributing Company Common Stock
    'KSU$': 10,  # (N) Kansas City Southern Preferred Stock
    'MKL': 10,  # (N) Markel Corporation Common Stock
    'NMK$B': 10,  # (N) Niagara Mohawk Holdings, Inc. Preferred Stock
    'NMK$C': 10,  # (N) Niagara Mohawk Holdings, Inc. Preferred Stock
    'NVR': 10,  # (N) NVR, Inc. Common Stock
    'SEB': 1,  # (A) Seaboard Corporation Common Stock
}


class TimeUnit(Enum):
    minute = auto()
    hour = auto()
    day = auto()
    week = auto()
    month = auto()
    year = auto()


class PolygonDataSource:
    NO_QUERY_LIMIT = 1_000_000

    def __init__(
            self, name="polygon", cache_dir=tempfile.TemporaryDirectory(prefix='Polygon').name, key=POLYGON_API_KEY):
        self.name = name
        self.cache_dir = cache_dir
        self.key = key
        self.log = logging.getLogger(self.name)

        self.raw_trade_conds = None
        self.trade_cond_map = dict()

        self.raw_quote_conds = None
        self.quote_cond_map = dict()

        self.raw_exchanges = None
        self.exchange_map = dict()
        self.exchange_mics = set()

        self.__load_caches()

    # load up caches
    def __load_caches(self):
        self.log.info("load_caches: %s" % self.cache_dir)
        if not os.path.exists(self.cache_dir):
            os.makedirs(self.cache_dir)
        self.__load_trade_conditions()
        self.__load_quote_conditions()
        self.__load_exchanges()

    def __load_trade_conditions(self):
        self.raw_trade_conds, source = self.__cached_data_load('trade_conds.json',
                                                               lambda: ws_get_conditions("trades", key=self.key))

        for k, v in self.raw_trade_conds.items():
            self.trade_cond_map[int(k)] = v
        self.log.info("load_trade_conditions: %d items  (%s)", len(self.trade_cond_map.keys()), source)

    def __load_quote_conditions(self):
        self.raw_quote_conds, source = self.__cached_data_load('quote_conds.json',
                                                               lambda: ws_get_conditions("quotes", key=self.key))

        for k, v in self.raw_quote_conds.items():
            self.quote_cond_map[int(k)] = v
        self.log.info("load_quote_conditions: %d items  (%s)", len(self.quote_cond_map.keys()), source)

    def __load_exchanges(self):
        self.raw_exchanges, source = self.__cached_data_load('exchanges.json', lambda: ws_get_exchanges(key=self.key))

        for exch in self.raw_exchanges:
            mic = exch.get('mic')
            if mic:
                # TODO - talk to polygon about this...missing OMX BX and BX Options/ETF
                if mic == 'XNAS':
                    mic = 'XBOS'
                if mic == 'PHLX':
                    mic = 'XPHL'
                self.exchange_map[exch['id']] = mic
        for k, v in self.exchange_map.items():
            self.exchange_mics.add(v)
        self.log.info("load_exchanges: %d items  (%s)", len(self.exchange_map.keys()), source)

    def __cached_data_load(self, cache_file, cache_proc):
        raw_json = None
        source = None
        try:
            raw_json = self.load_json(cache_file)
            if raw_json:
                source = "cache_file"
        except ValueError as e:
            self.log.warning("%s loading cached file (%s)...continuing" % (e, cache_file))

        if raw_json is None:
            raw_json = cache_proc()
            self.save_json(cache_file, raw_json)
            self.log.debug("saved results for %s" % cache_file)
            source = "web_svc"
        return raw_json, source

    def validate_venue(self, venue_mic, default_val=None):
        """ check that venue is known exchnage otherwise return None """
        return venue_mic if venue_mic in self.exchange_mics else default_val

    def get_nbbo_asof(self, sym, time, convert=False):
        """ Get the nbbo as-of given time """
        self.log.info("get_nbbo_asof(%s,%s)" % (sym, time))
        raw_data = ws_get_ticks(sym, time, None, 'nbbo', 1, True, self.key)
        if convert:
            nbbos = unmap_json_fields(raw_data, self.format_nbbo)
            return nbbos[0] if len(nbbos) > 0 else None
        else:
            return raw_data

    def get_nbbo_asof_parallel(self, sym, times, workers):
        """
        Get the nbbo as-of given times
        :param sym:
        :param times:
        :param workers:
        :return: dict of times to nbbo result (nbbo or None or Error)
        """
        self.log.info("get_nbbo_asof_parallel(%s,%s(%d),workers=%d)" % (sym, times, len(times), workers))

        nbbo_tasks = [(time, self.get_nbbo_asof, [sym, time, True]) for time in times]
        return parallel_exec(nbbo_tasks, workers=workers, raise_errors=True)

    def get_nbbo(self, sym, start, end, limit=10, convert=False):
        self.log.info("get_nbbo(%s,%s to %s,%s)" % (sym, start, end, limit))
        raw_data = ws_get_ticks(sym, start, end, 'nbbo', limit, False, self.key)
        if convert:
            return unmap_json_fields(raw_data, self.format_nbbo)
        else:
            return raw_data

    def format_nbbo(self, nbbo):
        # epoch to datetime
        self.__convert_timestamp(nbbo, 'sip_timestamp', True)
        self.__convert_timestamp(nbbo, 'participant_timestamp', True)
        self.__convert_timestamp(nbbo, 'trf_timestamp', True)
        # exchanges
        self.__convert_exchange(nbbo, 'bid_exchange')
        self.__convert_exchange(nbbo, 'ask_exchange')
        # conditions
        self.__convert_nbbo_conditions(nbbo, 'conditions')
        # lots to shares
        symbol = nbbo['symbol']
        self.__convert_lots(nbbo, symbol, 'bid_size')
        self.__convert_lots(nbbo, symbol, 'ask_size')

    def get_bars(self, sym, start, end, time_interval=1, time_unit=TimeUnit.minute, convert=True):
        """
        Get barred data for a symbol
        :param sym:
        :param start: datetime
        :param end: datetime, at least 1 day delta from start
        :param time_interval: length of the bar
        :param time_unit: minute|hour|day|week|month|year
        :param convert: True to format the output, otherewise return raw values
        :return:
        """
        self.log.info("get_bars(%s,%s-%s,%s %s)" % (sym, start, end, time_interval, time_unit.name))
        raw_data = ws_get_agg_v2(sym, start, end, time_interval, time_unit)
        if convert:
            return unmap_json_fields(raw_data, self.format_bar, AGG_FIELD_MAP)
        else:
            return raw_data

    def format_bar(self, quote):
        # epoch to datetime
        self.__convert_timestamp(quote)

    def get_quotes(self, sym, date, limit=10, convert=True):
        self.log.info("get_quotes(%s,%s,%s)" % (sym, date, limit))
        raw_quotes = ws_get_hist(sym, date, 'quotes', limit, self.key)
        if convert:
            return unmap_json_fields(raw_quotes, self.format_quote)
        else:
            return raw_quotes

    def format_quote(self, quote):
        # numeric id to MIC  (e.g. 10 -> XNYS)
        self.__convert_exchange(quote, 'bidexchange')
        self.__convert_exchange(quote, 'askexchange')
        # epoch to datetime
        self.__convert_timestamp(quote)
        # convert condition
        self.__convert_quote_condition(quote)

    def get_trades_v2(self, sym, start, end, limit=10, convert=True, fields_to_remove=('correction', 'orig_id')):
        self.log.info("get_trades_v2(%s,%s-%s,%s)" % (sym, start, end, limit))
        raw_trades = ws_get_ticks(sym, start, end, 'trades', limit, False, self.key)
        if convert:
            trades_corrections = unmap_json_fields(raw_trades, self.format_trade_v2)
            trades = list(filter(lambda x: x['correction'] is None, trades_corrections))
            for trade in trades:
                for fn in fields_to_remove:
                    del trade[fn]
            self.log.info(f'get_trades_v2: Found {len(trades)} trades...')
            return trades
        else:
            return raw_trades

    def format_trade_v2(self, trade):
        # epoch to datetime
        self.__convert_timestamp(trade, 'sip_timestamp', True)
        self.__convert_timestamp(trade, 'participant_timestamp', True)
        self.__convert_timestamp(trade, 'trf_timestamp', True)
        # numeric id to MIC  (e.g. 10 -> XNYS)
        self.__convert_exchange(trade, 'exchange')
        # condition decode
        conds = []
        conditions = trade.get('conditions')
        if conditions:
            for cond_code in conditions:
                cond_val = self.trade_cond_map.get(cond_code)
                conds.append(cond_val)
        trade.pop('conditions', None)
        trade['conds'] = conds
        trade['flags'] = self.__convert_conditions_to_flags(conds, trade)

    def get_trades(self, sym, date, limit=10, convert=True):
        self.log.info("get_trades(%s,%s,%s)" % (sym, date, limit))
        raw_trades = ws_get_hist(sym, date, 'trades', limit, self.key)
        if convert:
            return unmap_json_fields(raw_trades, self.format_trade)
        else:
            return raw_trades

    def format_trade(self, trade):
        # numeric id to MIC  (e.g. 10 -> XNYS)
        self.__convert_exchange(trade, 'exchange')
        # epoch to datetime
        self.__convert_timestamp(trade)
        # condition decode
        conds = [
            self.__convert_trade_condition(trade, 'condition1'),
            self.__convert_trade_condition(trade, 'condition2'),
            self.__convert_trade_condition(trade, 'condition3'),
            self.__convert_trade_condition(trade, 'condition4'),
        ]
        trade['conds'] = conds
        trade['flags'] = self.__convert_conditions_to_flags(conds, trade)

    def __convert_conditions_to_flags(self, conds, trade):
        """
        add flags
         - p = (last) price setting
         - v = volume eligible
         - b - block
         - c - chartable...similar to p but allows things like oddlots through
        :param conds:
        :param trade:
        :return: string containing any of the flags mentioned above
        """
        flags = ''
        if self.is_last_sale_eligible(conds):
            flags += 'p'
        if self.is_chartable(conds):
            flags += 'c'
        if self.is_volume_eligible(conds):
            flags += 'v'
        if 'v' in flags and self.is_block_trade(trade):
            flags += 'b'
        return flags

    def __convert_trade_condition(self, trade, cond_name):
        cond_code = trade.get(cond_name)
        cond_val = self.trade_cond_map.get(cond_code)
        if cond_code is not None:
            trade.pop(cond_name, None)
        return cond_val if cond_val else cond_code

    def __convert_quote_condition(self, quote, field_name='condition'):
        cond_code = quote.get(field_name)
        cond_val = self.quote_cond_map.get(cond_code)
        if cond_code is not None and cond_val is not None:
            quote[field_name] = cond_val

    def __convert_nbbo_conditions(self, nbbo, field_name='conditions'):
        conditions = nbbo.get(field_name)
        if conditions:
            vals = []
            for cond_code in conditions:
                cond_val = self.quote_cond_map.get(cond_code)
                vals.append(cond_val)
            nbbo[field_name] = vals

    @staticmethod
    def __convert_lots(nbbo, symbol, field_name):
        lots = nbbo.get(field_name)
        if lots:
            lot_size = NON_STANDARD_LOT_SIZES.get(symbol, 100)
            nbbo[field_name] = lots * lot_size

    def __convert_exchange(self, data, field_name):
        exchange_code = data.get(field_name)
        mic = self.exchange_map.get(exchange_code if isinstance(exchange_code, int) else int(exchange_code))
        data[field_name] = mic if mic else exchange_code

    @staticmethod
    def __convert_timestamp(data, field_name='timestamp', nanos=False):
        """converts timestamp to millis (float)"""
        if nanos:
            ts = data.get(field_name)
            if ts:
                data[field_name] = ts / (1000 * 1000)

    def save_json(self, file_name, json_data):
        file = os.path.join(self.cache_dir, file_name)
        with open(file, 'w') as fout:
            json.dump(json_data, fout, indent=4)

    def load_json(self, file_name):
        result = None
        file = os.path.join(self.cache_dir, file_name)
        if os.path.exists(file):
            if file_name.endswith(".gz"):
                with gzip.GzipFile(file, 'r') as fin:
                    result = json.loads(fin.read().decode('utf-8'))
            else:
                with open(file, 'r') as fin:
                    result = json.load(fin)
        return result

    @staticmethod
    def calc_bars(trades, groupby_freq='60s', time_field='timestamp'):
        LOG.info("calc_bars(trades=%d, %s)" % (len(trades), groupby_freq))
        # create data frame and convert time fields
        dt_start = datetime.datetime.now()
        df = pd.DataFrame(trades)
        ms_to_datetime64(df, time_field)

        # groups
        grouper = pd.Grouper(key=time_field, freq=groupby_freq)

        # all trade stats
        cnt_agg = df.groupby(grouper)[time_field].agg([('n', 'count')])

        # lse stats
        df_lse = df[df['flags'].str.contains('p')]
        lse_agg = df_lse.groupby(grouper)['price'].agg([
            ('open', 'first'),
            ('high', 'max'),
            ('low', 'min'),
            ('close', 'last')])
        lse_agg.dropna(inplace=True)

        # ve stats
        df_ve = df[df['flags'].str.contains('v')]
        ve_agg = df_ve.groupby(grouper)['size'].agg([('volume', 'sum')])

        # left join on prices.  fill NaN...happens in bin with last trade (CorrectedConsolidatedClosePrice)
        df_bars = lse_agg.join(ve_agg).join(cnt_agg)
        df_bars.fillna(value=0.0, inplace=True)

        # reset index numbering
        df_bars.reset_index(inplace=True)

        # valid_bins.columns = [' '.join(col).strip() for col in valid_bins.columns.values]
        dt_end = datetime.datetime.now()
        LOG.info("calc_bars: %d trades -> %d bars, duration=%s" %
                 (len(trades), len(df_bars),
                  str(dt_end - dt_start)))
        return df_bars

    @staticmethod
    def calc_lse_price(trade):
        flags = trade.get('flags')
        return trade['price'] if flags and 'p' in flags else None

    @staticmethod
    def calc_lse_size(trade):
        flags = trade.get('flags')
        return trade['size'] if flags and 'p' in flags else 0

    @staticmethod
    def calc_ve_size(trade):
        flags = trade.get('flags')
        return trade['size'] if flags and 'v' in flags else 0

    @staticmethod
    def is_last_sale_eligible(cond_array):
        for cond in cond_array:
            if cond in NON_LAST_SALE_ELIGIBLE_CONDS:
                return False
        return True

    @staticmethod
    def is_chartable(cond_array):
        for cond in cond_array:
            if cond in NON_CHARTABLE_ELIGIBLE_CONDS:
                return False
        return True

    @staticmethod
    def is_volume_eligible(cond_array):
        for cond in cond_array:
            if cond in NON_VOLUME_ELIGIBLE_CONDS:
                return False
        return True

    @staticmethod
    def is_block_trade(trade):
        px = trade['price']
        sz = trade['size']
        return px and sz and (sz >= 10000 or px * sz >= 200000)

    def get_nbbo_dates(self, convert=True):
        res = ws_get_stats(querytype="nbbo", key=self.key)
        dates = res['results']
        return [datetime.datetime.strptime(date, '%Y-%m-%d').date() for date in dates] if convert else dates

    @staticmethod
    def agg_trades(trades, agg_field):
        df = pd.DataFrame(trades)
        df['notional'] = df['price'] * df['size']

        # all
        df_all = df.groupby(by=agg_field)['size'].agg([('n', 'count'), ('v_total', 'sum')])
        df_vwap = df.groupby(by=agg_field)['notional'].agg([('notional', 'sum')])

        # lse
        df_lse = df[df['flags'].str.contains('p')]
        df_lse_sz = df_lse.groupby(by=agg_field)['size'].agg([('n_lse', 'count'), ('v_lse', 'sum')])
        df_lse_px = df_lse.groupby(by=agg_field)['price'].agg([('px_min', 'min'), ('px_max', 'max')])
        df_lse_vwap = df_lse.groupby(by=agg_field)['notional'].agg([('notional_lse', 'sum')])

        # blocks
        df_blocks = df[df['flags'].str.contains('b')]
        df_blocks = df_blocks.groupby(by=agg_field)['size'].agg([('n_blocks', 'count'), ('v_blocks', 'sum')])

        # OddLots
        df_oddlot = df[df['conds'].apply(lambda c: 'OddLot' in c)]
        df_oddlot = df_oddlot.groupby(by=agg_field)['size'].agg([('n_oddlot', 'count'), ('v_oddlot', 'sum')])

        # join all frames then do more column calcs
        df_res = df_all
        for df_data in [df_lse_sz, df_lse_px, df_blocks, df_oddlot, df_vwap, df_lse_vwap]:
            df_res = df_res.join(df_data)
        df_res['v_non_lse'] = df_res['v_total'] - df_res['v_lse']
        df_res['px_vwap'] = df_res['notional'] / df_res['v_total']
        df_res['px_vwap_lse'] = df_res['notional_lse'] / df_res['v_lse']

        # pretty up the data
        df_res.fillna(0.0, inplace=True)
        df_res = df_res.astype({
            'n': int, 'n_lse': int, 'n_blocks': int, 'n_oddlot': int,
            'v_total': int, 'v_lse': int, 'v_non_lse': int, 'v_oddlot': int, 'v_blocks': int
        })
        del df_res['notional']
        del df_res['notional_lse']
        df_res = df_res[[
            'n', 'n_lse', 'n_blocks', 'n_oddlot',
            'v_total', 'v_lse', 'v_blocks', 'v_oddlot', 'v_non_lse',
            'px_min', 'px_max', 'px_vwap', 'px_vwap_lse'
        ]]

        return df_res

    @staticmethod
    def calc_venue_summary(trades):
        dt_start = datetime.datetime.now()

        by_ex = PolygonDataSource.agg_trades(trades, 'exchange')
        total = PolygonDataSource.agg_trades(trades, 'symbol')
        total.rename(index={total.index[0]: 'TOTAL'}, inplace=True)
        summary = pd.concat([by_ex, total])
        summary['mkt_share'] = summary['v_total'] / total['v_total'][0]

        # flatten and rename cols
        summary.reset_index(inplace=True)
        summary.rename(index=str, columns={'index': 'exchange'}, inplace=True)

        dt_end = datetime.datetime.now()

        LOG.info("calc_venue_summary: %d trades -> %d rows, duration=%s" %
                 (len(trades), len(summary),
                  str(dt_end - dt_start)))
        return summary


def unmap_json_fields(json_data, post_processor=None, override_map=None):
    result_list = []
    symbol = json_data.get('symbol')
    if symbol is None:
        symbol = json_data.get('ticker')
    field_map = override_map if override_map else json_data.get('map')
    ticks = json_data.get('ticks')
    if ticks is None:
        ticks = json_data.get('results')
    if symbol and field_map and ticks:
        for tick in ticks:
            item = dict()
            item['symbol'] = symbol
            for short_name, long_name in field_map.items():
                if isinstance(long_name, dict):
                    # new format: { name:.., type:... }
                    long_name = long_name['name']
                item[long_name] = tick.get(short_name)
            if post_processor is not None:
                post_processor(item)
            result_list.append(item)
    return result_list


def ws_get_conditions(tick_type, key=POLYGON_API_KEY):
    query = 'https://api.polygon.io/v1/meta/conditions/{}?apikey={}'.format(tick_type, key)
    return ws_query_json(query)


def ws_get_exchanges(key=POLYGON_API_KEY):
    query = 'https://api.polygon.io/v1/meta/exchanges?apikey={}'.format(key)
    return ws_query_json(query)


def ws_get_hist(sym, ts, tick_type='trades', limit=10, key=POLYGON_API_KEY):
    date = ts.strftime("%Y-%m-%d")
    offset = int(1000 * ts.timestamp())
    query = 'https://api.polygon.io/v1/historic/{}/{}/{}?offset={}&limit={}&apikey={}'.format(
        tick_type, sym, date, offset, limit, key)
    return ws_query_json(query)


def ws_get_agg_v2(sym, start, end, time_interval, time_unit, key=POLYGON_API_KEY):
    start_date = start.strftime("%Y-%m-%d")
    end_date = end.strftime("%Y-%m-%d")
    # GET / v2 / aggs / ticker / {ticker} / range / {multiplier} / {timespan} / {fr
    query = 'https://api.polygon.io/v2/aggs/ticker/{}/range/{}/{}/{}/{}?apikey={}'.format(
        sym, time_interval, time_unit.name, start_date, end_date, key)
    return ws_query_json(query)


def ws_get_ticks(sym, start, end, querytype, limit=10, reverse=False, key=POLYGON_API_KEY):
    date_str = start.strftime("%Y-%m-%d")
    start_ns = int(1000 * 1000 * 1000 * start.timestamp())
    end_ns = int(1000 * 1000 * 1000 * end.timestamp()) if end is not None else None

    if limit <= POLYGON_QUERY_LIMIT:
        res_json = ws_get_ticks_chunk(sym, date_str, start_ns, end_ns, querytype, limit, reverse, key)
    else:
        all_ticks = []
        res_json = {}
        remaining = limit
        while remaining > 0:
            chunk_limit = min(remaining, POLYGON_QUERY_LIMIT)
            res_json = ws_get_ticks_chunk(sym, date_str, start_ns, end_ns, querytype, chunk_limit, reverse, key)
            ticks = res_json['results']
            all_ticks.extend(ticks)

            tick_count = len(ticks)
            if tick_count < chunk_limit:
                # done...got less than limit
                LOG.debug("tick_count=%d...BREAKING OUT cuz < %d", tick_count, chunk_limit)
                break
            else:
                # adjust for next query.  N.B. t = sip_timestamp
                start_ns = ticks[-1]['t']
                remaining -= tick_count
        res_json['results'] = all_ticks
        res_json['results_count'] = len(all_ticks)
        LOG.info("ws_get_ticks_chunked(%s, %s, %s, %s, limit=%d, results=%d)",
                 sym, start, end, querytype, limit,  len(all_ticks))
    return res_json


def ws_get_ticks_chunk(sym, date_str, start_ns, end_ns, querytype, limit=10, reverse=False, key=POLYGON_API_KEY):
    query = 'https://api.polygon.io/v2/ticks/stocks/{}/{}/{}?timestamp={}&limit={}&reverse={}&apikey={}' \
        .format(querytype, sym, date_str, start_ns, limit, reverse, key)
    if end_ns is not None:
        query += "&timestamplimit=" + str(end_ns)
    return ws_query_json(query)


def ws_get_stats(querytype="nbbo", key=POLYGON_API_KEY):
    query = 'https://api.polygon.io/v2/stats/ticks/%s?apikey=%s' % (querytype, key)
    return ws_query_json(query)


def ws_query_json(query, timeout=POLYGON_QUERY_TIMEOUT):
    t1 = datetime.datetime.now()
    try:
        # TODO - add more result checking and raise errors appropriately
        LOG.debug("REQUEST(%s)" % query)
        with requests.get(query, timeout=timeout) as res:
            if res.status_code != 200:
                raise ValueError(res.json())
            return res.json()
    finally:
        LOG.info(f'Polygon request time [{datetime.datetime.now() - t1}] for [{query}]')

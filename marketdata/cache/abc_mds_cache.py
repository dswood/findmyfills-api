#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

from abc import ABC, abstractmethod


class AbstractMDSCache(ABC):
    @abstractmethod
    def get_cache(self, table, sym, start_millis, end_millis):
        """
        Get cached subset of [start, end) for sym/type
        :param table: string identifying type of data being queried (max 10 chars)
        :param sym: symbol (max 12 chars)
        :param start_millis: start time in millis since epoch
        :param end_millis: end time in millis since epoch
        :return: {(start1, end1): data1, (start2, end2): data2, ...}
            Note that the start/end ranges may be discontiguous and an incomplete subset of the requested start/end.
            Empty list is returned if no cache found.
        """
        pass

    @abstractmethod
    def set_cache(self, table, sym, start_millis, end_mills, data):
        """
        Saves cached data. Does not check if data already exists.
        :param table: string identifying type of data being queried (max 10 chars)
        :param sym: symbol (max 12 chars)
        :param start_millis: start time in millis since epoch
        :param end_mills: end time in millis since epoch
        :param data: string data
        :return: No return value
        """
        pass

    @abstractmethod
    def del_cache(self, table, sym, *start_times):
        """
        Deletes cached data for provided sym/start_times
        :param table: string identifying type of data being queried (max 10 chars)
        :param sym: symbol (max 12 chars)
        :param start_times: list of start_millis
        :return: No return value
        """
        pass


class NullMDSCache(AbstractMDSCache):
    """No-op cache implementation"""

    def get_cache(self, table, sym, start_millis, end_millis):
        return {}

    def set_cache(self, table, sym, start_millis, end_mills, data):
        pass

    def del_cache(self, table, sym, *start_times):
        pass

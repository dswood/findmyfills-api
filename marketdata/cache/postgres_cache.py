#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import logging
import os

from sqlalchemy import MetaData, Table, Column, String, create_engine, DateTime, and_, inspect
from sqlalchemy.dialects.postgresql import JSONB, insert
from sqlalchemy.sql import select

from marketdata.cache.abc_mds_cache import AbstractMDSCache
from util import Singleton, datetime_to_millis, millis_to_datetime, measure_time


@Singleton
class PostgresCache(AbstractMDSCache):
    """
    References:
    - https://cloud.google.com/sql/docs/postgres/connect-app-engine
    - https://cloud.google.com/appengine/docs/flexible/python/using-cloud-sql-postgres
    - https://blog.codeship.com/unleash-the-power-of-storing-json-in-postgres/
    """

    def __init__(self, connection_string=None):
        self.log = logging.getLogger(__name__)
        if connection_string is None:
            self.db = create_engine(self.get_connection_string_from_env())
        else:
            self.db = create_engine(connection_string)
        self.meta = MetaData(self.db)
        self.tables = {}
        self.log.info("Created Cache: db_tables=%s", self.get_table_names())

    @staticmethod
    def get_connection_string_from_env():
        cs = os.environ.get("FMF_POSTGRES_CONNECTION_STRING")
        if cs is None:
            raise ValueError("FMF_POSTGRES_CONNECTION_STRING is not set")
        return cs

    def get_table_names(self):
        inspector = inspect(self.db)
        return inspector.get_table_names()

    def has_table(self, table_name):
        tn = self.get_table_names()
        exists = table_name in tn
        self.log.debug(f"has_table({table_name}, names={tn})={exists}")
        return exists

    @measure_time
    def get_table(self, table_name):
        """
        get table object from db.  if it doesn't exist, create one
        :return:
        """
        result = self.tables.get(table_name)
        if result is None:
            if self.has_table(table_name):
                db_table = Table(table_name, self.meta, autoload=True)
                self.log.debug(f"get_table({table_name}) - return existing")
            else:
                db_table = Table(
                    table_name,
                    self.meta,
                    Column('symbol', String, primary_key=True),
                    Column('start_time', DateTime(timezone=True), primary_key=True),
                    Column('end_time', DateTime(timezone=True), index=True),
                    Column('json_data', JSONB)
                )
                self.log.info(f"get_table({table_name}) - CREATED table")
                db_table.create(self.db)
            self.tables[table_name] = db_table
            result = db_table
        return result

    def drop_table(self, table_name):
        if self.has_table(table_name):
            table = self.get_table(table_name)
            table.drop(self.db)
            self.meta.remove(table)
            self.tables.pop(table_name, None)
            self.log.info(f"drop_table({table_name}) - DROPPED")
        else:
            self.log.info(f"drop_table({table_name}) - no-op")

    def get_all_rows(self, table):
        rows = []
        results = None
        with self.db.connect() as conn:
            try:
                results = conn.execute(select([table]))
                for row in results:
                    rows.append(dict(row))
            finally:
                if results is not None:
                    results.close()
        return rows

    def list_table_rows(self, desc, table):
        self.log.info(desc)
        for row in self.get_all_rows(table):
            self.log.info(row)

    @measure_time
    def get_cache(self, table_name, sym, start_millis, end_millis):
        """
        Get cached subset of [start, end) for sym/type
        :param table_name: string identifying type of data being queried (max 10 chars)
        :param sym: symbol (max 12 chars)
        :param start_millis: start time in millis since epoch
        :param end_millis: end time in millis since epoch
        :return: {(start1, end1): data1, (start2, end2): data2, ...}
            Note that the start/end ranges may be discontiguous and an incomplete subset of the requested start/end.
            Empty list is returned if no cache found.
        """
        data = {}

        table = self.get_table(table_name)
        stmt = select([table]).where(
            and_(
                table.c.symbol == sym,
                table.c.start_time < millis_to_datetime(end_millis),
                table.c.end_time > millis_to_datetime(start_millis)
            )
        )
        with self.db.connect() as conn:
            results = conn.execute(stmt)
            for row in results:
                st_ms = datetime_to_millis(row['start_time'])
                et_ms = datetime_to_millis(row['end_time'])
                data[(st_ms, et_ms)] = row['json_data']

        return data

    @measure_time
    def set_cache(self, table_name, sym, start_millis, end_millis, data):
        """
        Saves cached data. Does not check if data already exists.  Upserts data
        :param table_name: string identifying type of data being queried (max 10 chars)
        :param sym: symbol (max 12 chars)
        :param start_millis: start time in millis since epoch
        :param end_millis: end time in millis since epoch
        :param data: string data
        :return: No return value
        """

        table = self.get_table(table_name)
        st = millis_to_datetime(start_millis)
        et = millis_to_datetime(end_millis)

        # https://docs.sqlalchemy.org/en/13/dialects/postgresql.html#insert-on-conflict-upsert
        stmt = insert(table).values(symbol=sym, start_time=st, end_time=et, json_data=data)
        upsert = stmt.on_conflict_do_update(
            index_elements=['symbol', 'start_time'],
            set_=dict(end_time=et, json_data=data)
        )
        with self.db.connect() as conn:
            conn.execute(upsert)

    def del_cache(self, table_name, sym, *start_times):
        """
        Deletes cached data for provided sym/start_times
        :param table_name: string identifying type of data being queried (max 10 chars)
        :param sym: symbol (max 12 chars)
        :param start_times: list of start_millis
        :return: No return value
        """
        table = self.get_table(table_name)
        start_datetimes = list(map(lambda x: millis_to_datetime(x), start_times))
        self.log.info(start_datetimes)
        statement = table.delete().where(and_(
            table.c.symbol == sym,
            table.c.start_time.in_(start_datetimes)
        ))
        with self.db.connect() as conn:
            conn.execute(statement)

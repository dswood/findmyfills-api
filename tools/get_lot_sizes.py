#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#

import os

import requests


def get_data(use_cache=True):
    if use_cache:
        cached_file = os.path.join(os.path.dirname(__file__), 'tests/test_data/nasdaqtraded.txt')
        if os.path.exists(cached_file):
            with open(cached_file, 'r') as f:
                return f.read()

    url = "http://www.nasdaqtrader.com/dynamic/SymDir/nasdaqtraded.txt"
    with requests.get(url) as res:
        if res.status_code != 200:
            raise ValueError(res.json())
        return res.text


def generate_dict():
    res = get_data(False)
    print(len(res))

    lines = res.split("\n")
    print("lines=%d, header=%s, last=%s" % (len(lines), lines[0], lines[-2]))

    res = []
    data = lines[1:-2]
    for line in data:
        parts = line.split("|")
        if len(parts) == 12:
            sym = parts[1]
            name = parts[2]
            exch = parts[3]
            lot_size = int(parts[6])
            if lot_size != 100:
                res_str = "    '%s': %d,  # (%s) %s" % (sym, lot_size, exch, name)
                res.append(res_str)
    print("%d symbols with non-standard lot sizes" % len(res))

    print("\nNON_STANDARD_LOT_SIZES = {")
    res.sort()
    for r in res:
        print(r)
    print("}")


generate_dict()

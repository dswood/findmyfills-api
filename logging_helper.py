#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#

import logging
import os
import sys

import flask
from flask import logging as f_logging

from util import get_gae_service, get_gae_version


class RequestFormatter(logging.Formatter):
    def format(self, record):
        record.url = (' ' + flask.request.path) if flask.has_request_context() else ''
        return super(RequestFormatter, self).format(record)


def enhance_logger(app, debug):
    # are we in debug mode?
    app.debug = debug
    log_level = os.environ.get('LOGLEVEL', 'DEBUG' if app.debug else 'INFO')
    # create a request-aware formatter
    formatter = RequestFormatter('[%(asctime)s %(levelname)s %(module)s%(url)s]: %(message)s')
    # enhance flask logging handler
    handler = f_logging.default_handler
    handler.setFormatter(formatter)
    handler.setLevel(log_level)
    # set up app.logger and remove default_handler
    app.logger.setLevel(log_level)
    app.logger.removeHandler(handler)
    # enhance/setup general logging
    root_logger = logging.getLogger()
    root_logger.setLevel(log_level)
    if not root_logger.hasHandlers():
        root_logger.addHandler(handler)


def log_startup(app):
    app_version = get_gae_version()
    app_service = get_gae_service()
    app.logger.info(f"Application loaded. Service: [{app_service}], Version: [{app_version}], Config: {app.config}")


def init_logging(level_name='INFO'):
    if not logging.getLogger().hasHandlers():
        formatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setFormatter(formatter)
        logging.getLogger().addHandler(console_handler)
    logging.getLogger().setLevel(logging.getLevelName(level_name))


def log_list(logger, list_to_log, limit, reverse=False):
    i = 0
    if reverse:
        list_to_log = list_to_log.copy()
        list_to_log.reverse()
    for item in list_to_log:
        logger.info(item)
        i += 1
        if i >= limit:
            break

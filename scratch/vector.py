#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#
import logging

import pandas as pd

from polygon import PolygonDataSource, unmap_json_fields
from util import ms_to_datetime64, measure_time

ds = PolygonDataSource(cache_dir='../tests/test_data')
logging.basicConfig(level='INFO')


def load_trades():
    raw_trades = ds.load_json('trades_v2.json')
    trades = unmap_json_fields(raw_trades, ds.format_trade_v2)
    return trades


@measure_time
def fast():
    time_field = 'participant_timestamp'
    trades = load_trades()
    df = pd.DataFrame(trades)
    ms_to_datetime64(df, time_field)

    # groups
    grouper = pd.Grouper(key=time_field, freq='1s')

    # lse stats
    res = df.groupby(grouper)['price'].agg([
        ('open', 'first'),
        ('high', 'max'),
        ('low', 'min'),
        ('close', 'last')])
    res.dropna(inplace=True)
    return res


@measure_time
def slow():
    time_field = 'participant_timestamp'
    trades = load_trades()
    df = pd.DataFrame(trades)
    ms_to_datetime64(df, time_field)

    # groups
    grouper = pd.Grouper(key=time_field, freq='1s')

    # define agg function for apply
    def my_agg(x):
        series_px = x['price']
        if series_px.empty:
            return None
        names = {
            'open': series_px.iloc[0],
            'close': series_px.iloc[-1],
            'high': series_px.max(),
            'low': series_px.min()
        }
        return pd.Series(names, index=names.keys())

    res = df.groupby(grouper).apply(my_agg)
    res = res.dropna()
    return res


print(slow())
print(fast())

#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import datetime
import os

from google.cloud import bigtable
from google.cloud.bigtable.row_filters import ValueRangeFilter, ColumnQualifierRegexFilter, RowFilterChain, \
    ConditionalRowFilter, PassAllFilter

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'secrets/findmyfills-api-e36f6cff9b1d-admin.json'
mutate_timestamp = datetime.datetime.utcfromtimestamp(0)

client = bigtable.Client(project='findmyfills-api', admin=True)
instance = client.instance('findmyfills')

print('Tables are:', [(t.name, os.path.basename(t.name)) for t in instance.list_tables()])


def truncate_tables():
    for t in instance.list_tables():
        print(f'Truncating table {t.name}...')
        t.truncate(timeout=200)


# truncate_tables()
# exit()

table_id = 'greetings'
table = instance.table(table_id)

column_family_id = 'd'

if not table.exists():
    print('Creating the {} table'.format(table_id))
    table.create(column_families={column_family_id: None})
else:
    print(f"Table {table_id} already exists. Truncating...")
    table.truncate(timeout=200)

print('Writing some greetings to the table.')

num = 20
greetings = [f'g{i}' for i in range(num)]

column = 'd'.encode('utf-8')
end_col = 'e'.encode('utf-8')
rows = []
for i in range(num):
    row_key = f'greeting{num - i - 1:0>2}'
    row = table.row(row_key)
    row.set_cell(column_family_id, column, greetings[i], timestamp=mutate_timestamp)
    row.set_cell(column_family_id, end_col, (num - i - 1) * 10 + 5, timestamp=mutate_timestamp)
    rows.append(row)
    row_key = f'junk{num - i:0>2}'
    row = table.row(row_key)
    row.set_cell(column_family_id, column, row_key, timestamp=mutate_timestamp)
    rows.append(row)

table.mutate_rows(rows)

print('Getting a single greeting by row key')
key = b'greeting00'
t1 = datetime.datetime.now()
row = table.read_row(key, None)
print(f'Time taken: {datetime.datetime.now() - t1}')
print(row.cells[u'd'][b'd'][0].value.decode('utf-8'))

print('Creating filter')
start_value = (74).to_bytes(8, byteorder='big')
end_value = (116).to_bytes(8, byteorder='big')
print(start_value)
print(end_value)
vrf = ValueRangeFilter(start_value=start_value, end_value=end_value)
cqrf = ColumnQualifierRegexFilter(end_col)
fchain = RowFilterChain(filters=[cqrf, vrf])
fchain = ConditionalRowFilter(base_filter=fchain, true_filter=PassAllFilter(True))

print('Scanning for some greetings:')
t1 = datetime.datetime.now()
partial_rows = table.read_rows(start_key=b'greeting', end_key=b'greeting11.65', filter_=fchain)
# partial_rows = table.read_rows(start_key=b'greeting', end_key=b'greeting11.65')
# partial_rows = table.read_rows()
print(f'Time taken: {datetime.datetime.now() - t1}')
for row in partial_rows:
    # print(row.row_key, row.cells[u'd'][b'd'][0].value.decode('utf-8'))
    # print(row.to_dict())
    print(row.row_key, {k.decode(): v[0].value for k, v in row.to_dict().items()})
